from threednetmod.get_feature_distribution import get_feature_distribution
import numpy as np
from threednetmod.load_community_list import load_community_list
from threednetmod.make_scatterplots import make_scatterplots
import seaborn
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import os

def make_variance_distribution_violinplots(variance_distributions_list, settings, cell_type):

	#thresholds = [0, 70, 80, 90, 100]
	sizes = [1, 2, 3, 4, 5]
	filename = 'Violin_plot_sizekb_by_variancethresh_' + cell_type + '_'  + '_variance_threshold_100_AUC' + '_gammastep_' + settings['gamma_step'] +  '_num_part_' + settings['num_part'] +  settings['gamma_part'] + '_gammapart_' + settings['plateau'] + '_plateau.png'
	dir = 'output/variance_distribution_violins/'
	if not os.path.isdir(dir): os.makedirs(dir)
        seaborn.set_style("whitegrid")
        ax = seaborn.violinplot(data = variance_distributions_list, palette="muted", cut = 0, scale = 'count', inner = None)
        ax.set(xlabel='size stratum', ylabel='boundary variance')
	ax.set(ylim = (0, 60))
        plt.savefig(os.path.join(dir,filename), dpi = 900, bbox_inches = 'tight')
        plt.clf()
        plt.close()



if __name__ == '__main__':

	main()
