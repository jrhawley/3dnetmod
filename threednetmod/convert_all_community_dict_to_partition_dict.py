from threednetmod.convert_comm_coord_to_array import convert_comm_coord_to_array

def convert_all_community_dict_to_partition_dict(hybrid_community_dict, bin_location):
	"""
	hybrid_partitions_dict : nested dictionary with key = hybrid number --> dictionary with key = region number --> value with list of partitions
	"""
	print 'in convert_all_community_dict_to_partition_dict'
	hybrid_partitions_dict = {}
	for key in hybrid_community_dict:
		regions = []
		hybrid_partitions_dict[key]={}
		hybrid_communities = hybrid_community_dict[key]
		for hybrid_community in hybrid_communities:
			region = hybrid_community['region']
			if region not in regions:
				regions.append(region)
				hybrid_partitions_dict[key][region] = []
			hybrid_array = convert_comm_coord_to_array(hybrid_community, bin_location)
			hybrid_partitions_dict[key][region].append(hybrid_array)


	return hybrid_partitions_dict
