#from threednetmod.load_binned_counts import load_binned_counts
from threednetmod.load_communities import load_communities
#from threednetmod.load_pixelmap import load_pixelmap
#from threednetmod.plot_heatmap import plot_heatmap
#from threednetmod.plot_heatmap_two_community_sets import plot_heatmap
#from threednetmod.plot_heatmap import plot_heatmap
from threednetmod.plot_heatmap_w_classified_zoom_communites import plot_heatmap
from threednetmod.plot_heatmap_w_classified_zoom_communites_flip import plot_heatmap_flip
from threednetmod.get_colormap import get_colormap
import numpy as np
from threednetmod.flatten_counts_single_region_geometric import flatten_counts_single_region
#from threednetmod.parse_community_calls_by_region import parse_community_calls_by_region
from threednetmod.parse_community_calls_by_region import parse_netmod_community_calls_by_region
from threednetmod.parse_community_calls_by_region import parse_netmod_community_calls_by_region_abbreviated
from threednetmod.parse_community_calls_by_region import parse_netmod_community_calls_with_classification_by_region_abbreviated
from threednetmod.load_binned_counts import load_binned_counts
import sys
import os
from threednetmod.read_settings import read_settings
from threednetmod.counts_to_array import counts_to_array
from threednetmod.load_pixelmap import load_pixelmap
from threednetmod.load_region_start_end_coord import load_region_start_end_coord
from threednetmod.check_intersect import check_intersect

def get_zoom_region(zoom_coord,  region_coords):
	print 'zoom_coord: ', zoom_coord
	for region in region_coords:
		if zoom_coord['chrom'] == region_coords[region]['chrom']:
			if region_coords[region]['start'] < zoom_coord['start'] and region_coords[region]['end'] > zoom_coord['end']:
				print 'found region: ', region
				return region


def load_communities_w_classification(filename, pval_thresh):
        community_dict = {}
        regions = []
        input = open(filename, 'r')
        for line in input:
                if line.startswith('#'):
                        continue
                else:
                        temp = line.strip().split('\t')
                        region = temp[0]
                        if region not in regions:
                                community_dict[region] = []
                                regions.append(region)
                        start = int(temp[4])
                        end = int(temp[5])
                        left_pval = float(temp[8])
                        right_pval = float(temp[9])
                        if left_pval <= pval_thresh:
                                left_type = 'dynamic'
                        else:
                                left_type = 'constitutive'
                        if right_pval <= pval_thresh:
                                right_type = 'dynamic'
                        else:
                                right_type = 'constitutive'
                        community_dict[region].append((start, end, left_type, right_type))
        input.close()


        return community_dict



def find_communities_with_dynamic_boundaries(communities_w_pvals_file, pval_thresh):

	communities_with_dynamic_boundaries = []
	input = open(communities_w_pvals_file, 'r')
	for line in input:
                if line.startswith('#'):
                        continue
		else:
			temp = line.strip().split('\t')
			community = {}
			community['chr'] = temp[0]
			community['start'] = int(temp[1])
			community['stop'] = int(temp[2])
			left_pval = float(temp[3])
			right_pval = float(temp[4])
			pvalues = [left_pval, right_pval]
			community['pvalue'] = min(pvalues)
			if community['pvalue'] < pval_thresh:
				communities_with_dynamic_boundaries.append(community)

	print 'number of communities with dynamic boundaries: ', len(communities_with_dynamic_boundaries)
	return communities_with_dynamic_boundaries


def main():

	"""
	Adds a 4 bin buffer in start and stop to the input zoom coordinates
	"""
	settings = read_settings(sys.argv[1])
	sample = sys.argv[2] # cell type with the dynamic boundary of intersest
	buffer = int(sys.argv[3]) # number of bins plotted before and after the community of interest
	pval_thresh = float(sys.argv[4])
	chip = sys.argv[5] # true or false whether ChIP tracks should be plotted or not
	linewidth = float(sys.argv[6])



        if sample == settings['sample_1']:
                sample2 = settings['sample_3']
        elif sample == settings['sample_3']:
                sample2 = settings['sample_1']
        else:
                print 'final argument must be either', settings['sample_1']
                print 'or: ', settings['sample_3']
	comparison_type = sample + '_v_' + sample2
        #sample = settings['sample_1']




	strata = (0, 6000000)
	communities_w_pvals_file = 'output/classified_boundaries/results_files/' + 'Size_' + str(strata[0] // 1000) + 'kb_to_' + str(strata[1] // 1000) + 'kb_' + comparison_type + '_all_communities_w_pval_'  + '_gammastep_' + settings['gamma_step'] + '_hierarchy_' + settings['hierarchies'] + settings['gamma_part'] + '_gammapart_' + settings['plateau'] + '_plateau_' + settings['var_thresh1'] + '_' + str(settings['var_thresh2']) + '_' + str(settings['var_thresh3']) + '_' + str(settings['var_thresh4']) + '_' + str(settings['var_thresh5']) + '_merge_' + settings['merge'] + '_' + settings['comparison'] +  '_genomewide_' + settings['scale'] + '_min_size_' + settings['size_threshold'] +'.txt'

	communities_with_dynamic_boundaries = find_communities_with_dynamic_boundaries(communities_w_pvals_file, pval_thresh)

        ES_CTCF = 'input/chip/qnormed_ES_CTCF.bw'
        NPC_CTCF = 'input/chip/qnormed_NPC_CTCF.bw'
        ES_Smc1 = 'input/chip/qnormed_ES_Cohesin.bw'
        NPC_Smc1 = 'input/chip/qnormed_NPC_Cohesin.bw'
        ES_H3K27ac = 'input/chip/qnormed_ES_H3K27ac.bw'
        NPC_H3K27ac = 'input/chip/qnormed_NPC_H3K27ac.bw'
        dir = 'output/heatmaps_zoom_w_chip/'
        if not os.path.isdir(dir): os.makedirs(dir)
	for community in communities_with_dynamic_boundaries:
		chr = community['chr']
        	cell_type_input = chr + '_' + sample
        	cell_type1 = chr + sample
        	counts_file = 'input/' +  cell_type_input + "finalpvalues.counts"

        	#sample2 = settings['sample_3']
        	cell_type2_input = chr + '_' + sample2
        	cell_type2 = chr + sample2
        	counts_file2 = 'input/' +  cell_type2_input + "finalpvalues.counts"
		bed_filename = 'input/' + cell_type_input + "final.bed"
		counts,_ = counts_to_array(counts_file,bed_filename)
		counts2,_ = counts_to_array(counts_file2,bed_filename)
		pixelmap = load_pixelmap(bed_filename)
		region_coords, bin_size  = load_region_start_end_coord(bed_filename)




		str_zoom = community['chr'] +  ':' + str(community['start']-buffer*bin_size) + '-' + str(community['stop'] + buffer*bin_size)
		zoom_window = (str_zoom, str_zoom)

		label = sample + '_specific_boundary_pvalue_' + str(community['pvalue']) + '_' + chr + '_' + str(community['start']) + '_' + str(community['stop']) + '_buffer_' + str(buffer) + '_consistent_v_unique_linewidth_' + str(linewidth)
		zoom_dict = {}
		zoom_dict['chrom'] = chr
		zoom_dict['start'] = community['start']
		zoom_dict['end'] = community['stop']
		zoom_region = get_zoom_region(zoom_dict, region_coords)


		for region in [zoom_region]: 
			start_coord = pixelmap[region][0]['start']
			end_coord = pixelmap[region][-1]['end']
			celltype1_community_file = communities_w_pvals_file
			celltype1_communities = parse_netmod_community_calls_with_classification_by_region_abbreviated(celltype1_community_file, chr, start_coord, end_coord, float(settings['p_value']))

			print 'number of celltype1_communities: ', len(celltype1_communities)
		
			celltype2_community_file = 'output/unique_communities_thresholded_post/results_files/size_stratified/Communities_' + settings['num_part'] + '_partitions_' +settings['pctile_threshold'] + '_' + settings['pct_value'] + '_trash_community_removal_' + settings['diagonal_density'] + '_' + settings['consecutive_diagonal_zero'] + '_bad_regions_' + cell_type2  + '_gammastep_' + settings['gamma_step'] + '_hierarchy_' + settings['hierarchies'] + settings['gamma_part'] + '_gammapart_' + settings['plateau'] + '_plateau_thresholded_post_writing_' + settings['var_thresh1'] + '_' + str(settings['var_thresh2']) + '_' + str(settings['var_thresh3']) + '_' + str(settings['var_thresh4']) + '_' + str(settings['var_thresh5'])  + '_min_size_' + settings['size_threshold'] +'.txt'

			print 'celltype2 community file: ', celltype2_community_file

			celltype2_communities = parse_netmod_community_calls_by_region(celltype2_community_file, chr, start_coord, end_coord)

			print 'number of celltype2_communities: ', len(celltype2_communities)
			print 'celltype2_communities: ', celltype2_communities

			colorscale=(0.0,np.percentile(flatten_counts_single_region(counts[region], discard_nan=True),98))
			if chip == 'True':
				heatmap_filename =  dir + region + '_' + label + '_' + cell_type_input +  '_'  + 'CTCF_H3K27ac.png'
				plot_heatmap(counts[region], heatmap_filename, zoom_window=zoom_window,colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = celltype1_communities, primer_file = (bed_filename), tracks = [NPC_H3K27ac, ES_H3K27ac, NPC_CTCF, ES_CTCF], track_scales = "auto-pairs", show_track_labels = False, chipseq_track_line_width=linewidth)
				heatmap_filename =  dir + region + '_' + label + '_' + cell_type_input +  '_'  + 'CTCF_H3K27ac_FLIP.png' 
				plot_heatmap_flip(counts[region], heatmap_filename, zoom_window=zoom_window,colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = celltype1_communities, primer_file = (bed_filename), tracks = [NPC_H3K27ac, ES_H3K27ac, NPC_CTCF, ES_CTCF], track_scales = "auto-pairs", show_track_labels = False, chipseq_track_line_width=linewidth)


				heatmap_filename =  dir + region + '_' + label + '_' + cell_type2_input +  '_'  + 'CTCF_H3K27ac.png'
				plot_heatmap(counts2[region], heatmap_filename, zoom_window=zoom_window,colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, primer_file = (bed_filename), communities = celltype2_communities, tracks = [NPC_H3K27ac, ES_H3K27ac, NPC_CTCF, ES_CTCF], track_scales = "auto-pairs", show_track_labels = False, chipseq_track_line_width=linewidth)


                                heatmap_filename =  dir + region + '_' + label + '_' + cell_type2_input +  '_'  + 'CTCF_H3K27ac_FLIP.png'
                                plot_heatmap_flip(counts2[region], heatmap_filename, zoom_window=zoom_window,colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, primer_file = (bed_filename), communities = celltype2_communities, tracks = [NPC_H3K27ac, ES_H3K27ac, NPC_CTCF, ES_CTCF], track_scales = "auto-pairs", show_track_labels = False, chipseq_track_line_width=linewidth)


			else:
				heatmap_filename =  dir + region + '_' + label + '_' + cell_type_input + '.png'
				plot_heatmap(counts[region], heatmap_filename, zoom_window=zoom_window,colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = celltype1_communities, primer_file = (bed_filename))
				heatmap_filename =  dir + region + '_' + label + '_' + cell_type2_input + '.png'
				plot_heatmap(counts2[region], heatmap_filename, zoom_window=zoom_window,colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, primer_file = (bed_filename), communities = celltype2_communities)
	

if __name__ == '__main__':
	main()
	
