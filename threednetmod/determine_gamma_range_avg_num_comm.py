from __future__ import division, absolute_import, print_function
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as pyplot
import sys
import glob
import os
from threednetmod.read_settings import read_settings
from threednetmod.get_full_gammas import get_full_gammas
from threednetmod.counts_to_array import counts_to_array
from threednetmod.real_random_Qs import real_random_Qs
#from threednetmod.make_Q_v_gamma_plots import make_Q_v_gamma_plots
from threednetmod.make_Q_v_gamma_plots_simplified import make_Q_v_gamma_plots
#from threednetmod.save_gammas_in_range import save_gammas_in_range
from threednetmod.save_gammas_in_range_abbreviated import save_gammas_in_range
from threednetmod.get_random_Qs import get_random_Qs
from threednetmod.construct_nulls import construct_NG_null
import time
from threednetmod.real_random_Qs_avg import real_random_Qs_avg
from threednetmod.make_scatterplots import make_scatterplots
from threednetmod.make_scatterplots import make_scatterplots_zoom
from threednetmod.get_plateaus import get_plateaus
from threednetmod.save_avg_num_comm_v_gamma import save_avg_num_comm_v_gamma
from threednetmod.real_Qs_avg import real_Qs_avg
from threednetmod.determine_max_gamma_genomewide import determine_max_gamma

"""
Heidi Norton, Daniel Emerson

Determine the useful range of the structural resolution parameter, gamma, by computing modularity for a real and randomly rewired networks across a range of structural resolution parameter values.

Final useful gamma is determined as the gamma at which the modularity of the real network converges with modularity of the random network.

Assumes convergence has to occur after 1.5 gamma.

User defined settings should be specified in settings.txt

"""

def determine(chr, settings,sample):
    #settings = read_settings('settings_genomewide.txt')
    num_rand = int(settings['num_rand'])
    #cell_type_input = str(chr) + '_' + settings['sample_1']
    cell_type_input = str(chr) + '_' + sample

    counts = 'input/' +  cell_type_input + "finalpvalues.counts"
    #counts = 'input/' + settings['counts_file_1']
    bed = 'input/' +  cell_type_input + "final.bed"
    #bed = 'input/' + settings['bed_file']
    #cell_type = str(chr) + settings['sample_1']  #removing underscore for downstream writing files because cell_type is split on underscore
    cell_type = str(chr) + sample
    counts_as_arrays,bin_location = counts_to_array(counts, bed)
    gammas_finer = {}
    num_comm = {}
    real_Q_finer = {}
    random_Qs_finer = {}
    plateau_dict = {}
    start = time.time()
    gamma_step = float(settings['gamma_step'])

    try:
        x = 1 // int(1 / gamma_step)
    except ZeroDivisionError:
         print("Cannot exceed step size of 1 gamma")
         return 0

    max_gamma = determine_max_gamma(settings,counts_as_arrays.keys(),counts_as_arrays,cell_type,counts)

    for region in counts_as_arrays:
    #for region in ['HiCchr3058']:
         real_Q = 1
         gamma = 0.0
         gammas_finer[region] = []
         num_comm[region] = []
         real_Q_finer[region] = []
         random_Qs_finer[region] = []

         # Perform a finer sampling of gamma to get plateaus in avg # community v gamma
         gamma = 0
         mod = []
         #while gamma <= max(gammas[region]):
         A = counts_as_arrays[region]
         np.fill_diagonal(A, 0) # zero out the diagonal of the network to match random rewiring scenario
         NG = construct_NG_null(A) # construct the Newman  Girvan null model
         while gamma <= max_gamma:
             real_Q,num_communities  = real_Qs_avg(settings,A,NG,gamma)
             gammas_finer[region].append(gamma)
             num_comm[region].append(num_communities)
             real_Q_finer[region].append(real_Q)
             #random_Qs_finer[region].append(random_Qs)
             gamma = gamma + float(settings['gamma_step'])


         identifier = cell_type + '_' + region + '_' + settings['gamma_part']  + '_partitions_gamma_step_' + settings['gamma_step'] + '_gamma_step'
         # Make plots 
         if settings['plots'] == 'True':

             filename = 'Avg_num_comm_v_gamma_' + identifier + '.png'
             make_scatterplots(gammas_finer[region], num_comm[region], filename, 'output/determine_gamma/scatterplots/')

             filename = 'Avg_num_comm_v_gamma_zoom1' + identifier + '.png'
             make_scatterplots_zoom(gammas_finer[region], num_comm[region], filename, 'output/determine_gamma/scatterplots/', (2,3))


             filename = 'Avg_num_comm_v_gamma_zoom2' + identifier + '.png'
             make_scatterplots_zoom(gammas_finer[region], num_comm[region], filename, 'output/determine_gamma/scatterplots/', (3,4))

             filename = 'Avg_num_comm_v_Q_' + identifier + '.png'
             make_scatterplots(real_Q_finer[region], num_comm[region], filename, 'output/determine_gamma/scatterplots/')

             filename = 'Q_v_Avg_num_comm_' + identifier + '.png'
             make_scatterplots(num_comm[region], real_Q_finer[region], filename, 'output/determine_gamma/scatterplots/')

             filename = 'Q_v_gamma_' + identifier + '.png'
             make_scatterplots(gammas_finer[region], real_Q_finer[region], filename, 'output/determine_gamma/scatterplots/')


    #mean_diffs_dict, normed_diffs_dict = make_Q_v_gamma_plots(settings, cell_type, gammas_finer, real_Q_finer, random_Qs_finer)

    save_avg_num_comm_v_gamma(num_comm, gammas_finer, settings, cell_type)

    

    plateau_gammas = {}
    #for region in ['HiCchr3058']:
    for region in counts_as_arrays:
         plateau_gammas[region] = []
         plateau = get_plateaus(num_comm[region], gammas_finer[region], int(settings['plateau']))
         for avg_num_comm in plateau:
              if avg_num_comm > 1:   # only want to keep gammas that lead to at least 2 communities
                   plateau_gammas[region].append(np.mean(plateau[avg_num_comm]))

         print 'final gammas: '
         print plateau_gammas[region]
         plateau_gammas[region].sort() 
    save_gammas_in_range(settings, cell_type, plateau_gammas)



if __name__ == '__main__':
	main()
