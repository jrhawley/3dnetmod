import os
from threednetmod.read_seeds import read_seeds
import numpy as np
from threednetmod.construct_nulls import construct_NG_null
from threednetmod.assign_communities_gen_louvain import assign_sorted_communities_gen_louvain
from threednetmod.plot_partition import plot_partition
from threednetmod.make_scatterplots import make_scatterplots
from threednetmod.get_gammas import get_gammas
from threednetmod.get_partition_colormap import get_colormap
from threednetmod.get_gammas_from_plateau import get_gammas_from_plateau

def create_partitions(settings, counts_array, cell_type, region, seed_count, hybrid = 'none'):
	"""
	Creates partition block for counts array at every gamma in a range
	Saves image and text files for partition blocks at each gamma
	Makes a scatterplot of avg. num of communities vs gamma

	INPUTS
	------
	settings, dict of strings
		dictionary corresponding settings categories to their string values
	counts_array, ndarray
		a symmetrical array of counts
	region, str
		the region of the array
	seed_count, int
		the current counter of seed location
		
	OUTPUTS
	-------
	communities_dict, dict of ndarrays
		key = gamma, value = num_part x num_nodes 2D partition block
	seed_count, int
		an updated counter of seed location
	"""
	print '\npartioning %s region' %region
	assert not np.any(np.isnan(counts_array)) # check for nans
	cell_type = cell_type.split('_')[0]
	#gamma_file = 'gamma_dynamic_range_' + cell_type.split('_')[0] + '_' + region  + '_step_size_gamma_' + settings['gamma_step'] + str(eval(settings['switch']))  + '_adj.txt'
	avg_num_comm_file = 'Avg_num_comm_v_gamma_' + cell_type.split('_')[0] + '_' + region  + '_step_size_gamma_' + settings['gamma_step'] + str(eval(settings['switch']))  + '_adj_' + settings['gamma_part'] + '_gammapart.txt'

	gammas = get_gammas_from_plateau('output/gamma_dynamic_range/' + avg_num_comm_file, settings)
	np.savetxt('output/gamma_dynamic_range/gamma_dynamic_range_' + cell_type.split('_')[0] + '_' + region  + '_step_size_gamma_' + settings['gamma_step'] + str(eval(settings['switch']))  + '_adj_' + settings['gamma_part'] + '_gammapart_' + settings['plateau'] + '_plateau.txt', gammas)


	#gammas = get_gammas('output/gamma_dynamic_range/' + gamma_file)
	communities_dict = {}
	NG = construct_NG_null(counts_array) # null model
	num_comm = []
	gen_louvain_seeds = read_seeds(settings['seed_file'], settings['num_part']) # a list of int seeds
	for gamma in gammas:
		print 'gamma %.2f being partitioned' %gamma
		
		# Assign communities using the Louvain algorithm numpart times. 
		# Ci is a num_part x num_nodes 2D numpy array of community assignments. Each row represents a different community partition.
		# Q is a list of modularity values from each application of the algorithm
		[Ci, Q, seed_count] = assign_sorted_communities_gen_louvain(counts_array, NG, int(settings['num_part']), gamma, gen_louvain_seeds, seed_count)
		
		# Add partition block to dictionary
		communities_dict[gamma] = Ci
		
		# add mean number of communities at current gamma
		mean_number_of_communities = round(np.mean(Ci.max(axis=1)))
		num_comm.append(mean_number_of_communities)
		
		identifier = cell_type + '_' + region + '_' + settings['num_part'] + '_partitions_gamma_step_' + settings['gamma_step'] + settings['gamma_part'] + '_gammapart_' + settings['plateau'] + '_plateau'
		# Save image of partition across num_part for a given gamma
		if hybrid == 'none':
			if eval(settings["plots"]):
				filename = 'Partitions_across_num_part_' + identifier + '_' + str(gamma) + '.png'
				plot_partition(Ci, get_colormap(), filename, subdir='per_gamma')

		# Save text representation of partition across num_part for a given gamma
		filename = 'Partitions_per_gamma_' + identifier + '_' + str(gamma) + '.csv'
		#filename = 'Partitions_per_gamma_%s_%s_%.2f.csv' %(short_counts_filename, region, gamma)
		dir = 'output/partitions_blocks_per_gamma/'
		if not os.path.isdir(dir): os.makedirs(dir)
		np.savetxt(os.path.join(dir,filename), Ci)
	if hybrid == 'none':
		# plot average number of communities v gamma
		filename = 'Avg_num_comm_v_gamma_' + identifier + '.png'
		#filename = 'Avg_num_comm_v_gamma_%s_%s' %(short_counts_filename, region)
		if eval(settings["plots"]):
			make_scatterplots(gammas, num_comm, filename, 'output/scatterplots')
	
	return communities_dict, region, seed_count
