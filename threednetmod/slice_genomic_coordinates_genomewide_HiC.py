from __future__ import division, absolute_import, print_function
import os
import sys
import re
import random
import copy
import csv
import numpy as np

'''
     split counts into uniform regions with overlap and no deadzone removal.  Assumes HiC format
     for input counts and bed

'''


class MatrixMake:
      Maximum = 0
      Minimum = 100000
      logged = False

      def __init__(self, filename,filename2,start,stop,first,last,number,region,logged,stride,overlap):
           self.filename = filename  #input counts
           self.filename2 = filename2
           self.start = int(start)
           self.stop = int(stop)
           self.first = int(first)
           self.last = int(last)
           self.pcount = []
           self.list_of_bins = []
           self.list_of_regions = []
           self.number = number
           MatrixMake.logged = logged
           self.stop_processing = False
           self.stride = stride
           self.overlap = overlap
           self.end = 0

           for i in range(int(self.start),int(self.stop)+1):
               self.list_of_bins.append(i)

           MatrixMake.Maximum = int(self.last)
           MatrixMake.Minimum = int(self.first)

           self.Heatmap = np.empty(((self.stop-self.start)+1,(self.stop-self.start)+1))
           self.region = region
           self.readfile()
           self.fillemptydiags()
           self.pCountMake()


      def readfile(self):

           f = open(self.filename,"r+")
           counter = 0
           for lines in f:
                counter = counter + 1
                lines = lines.strip('\n')
                values = lines.split('\t')
                values = [i.strip(' ') for i in values]  
                self.test(values)
                if (lines == ""): #EOF
                     self.end = 1
                if self.stop_processing:  #keep from wasting too much time processing lines if exceeds the end
                     counter = -10
                     break
           f.close()

           if counter >= 0:  #end 
                self.end = 1

           f = open(self.filename2,"r+")
           processing = 1
           for lines in f:
               lines = lines.strip('\n')
               values = lines.split('\t')
               values = [i.strip(' ') for i in values]
               self.test2(values,processing)
           #    if MatrixMake.stop_processing:  #keep from wasting too much time processing lines if exceeds the end
           #         break
           f.close()

      def test(self,values):
           #test to see if input line from counts is suitable for storage.  Note HiC format - no splitting on '_'
           firstcol = values[0]
           secondcol = values[1]

           if ((int(firstcol) >= int(self.start)) and (int(firstcol) <= int(self.stop))):
                if ((int(secondcol) >= int(self.start)) and (int(secondcol) <= int(self.stop))):
                       bin1 = int(firstcol) - self.start
                       bin2 = int(secondcol) - self.start
                       if eval(MatrixMake.logged):
                            if float(values[2]) >= 1:
                                 #if int(self.number) > 1:
                                 #     print(self.number, " placing value ",float(values[2]))
                                 self.Heatmap[bin1][bin2] = np.log2(float(values[2]))
                                 self.Heatmap[bin2][bin1] = np.log2(float(values[2]))
                                 #if int(self.number) > 1:
                                 #     print("Heatmap [",bin1,"][[",bin2,"] = ",self.Heatmap[bin1][bin2])
                            else:
                                 self.Heatmap[bin1][bin2] = 0
                                 self.Heatmap[bin2][bin1] = 0
                       else:
                            if float(values[2]) >= 0: #remove neg value from consideration
                                 self.Heatmap[bin1][bin2] = float(values[2])
                                 self.Heatmap[bin2][bin1] = float(values[2])
                            else:
                                 self.Heatmap[bin1][bin2] = 0
                                 self.Heatmap[bin2][bin1] = 0

           if int(firstcol) > self.stop:
                self.stop_processing = True

      def test2(self,values,processing):
           #test to see if input line from bed is suitable for storage.  Note HiC format - no splitting on '_'

           forthcol = values[3]
           binstart = int(forthcol) - self.start
           digits = len(str(MatrixMake.Maximum))

           if (int(forthcol) in self.list_of_bins):
               self.list_of_regions.append([values[0],values[1],values[2],str(self.region) + str(self.number).zfill(3) + "_BIN_" + str(binstart).zfill(digits)])  #attach all rows of .bed input to matching bins

      def fillemptydiags(self):
          #fill in empty diagonals
          diag_of_interest = []
          #find empty diagonals
          for i in range(0,len(self.Heatmap)):
              #diagonal is all zeros
              if float(len(self.Heatmap.diagonal(i)[self.Heatmap.diagonal(i)==0])) / float(len(self.Heatmap.diagonal(i))) == 1:
                  diag_of_interest.append(i)
              else:
                  break #immediately exit, avoid looking at off diagonals far from diagonal
          if len(diag_of_interest) > 0:
               for col in range(len(diag_of_interest),1,-1):  #start at furthest from center diagonal
                    col_of_interest = diag_of_interest[col-1]
                    #only evaluate if there is a neighbor to right
                    while col_of_interest <= len(self.Heatmap) - (diag_of_interest[col-1]+1):
                          value = self.Heatmap[col_of_interest-1][col_of_interest + 1]
                          self.Heatmap[col_of_interest-1][col_of_interest] = value
                          self.Heatmap[col_of_interest][col_of_interest-1] = value
                          col_of_interest = col_of_interest + 1  
               #adjust center diagonal
               col_of_interest = 0
               while col_of_interest <= len(self.Heatmap) - 2:
                     value = self.Heatmap[col_of_interest][col_of_interest+1]
                     self.Heatmap[col_of_interest][col_of_interest] = value
                     col_of_interest = col_of_interest + 1

      def pCountMake(self):
          #store pcount data

          digits = len(str(MatrixMake.Maximum))
          temppcount =  []
          b = self.start + 1
          for i in range(0,len(self.Heatmap)):
               for a in range(self.start,b):
                   #print("self.Heatmap[i][2] = ", self.Heatmap[i][2])
                   self.pcount.append([str(self.region) + str(self.number).zfill(3) + "_BIN_" + str(i).zfill(digits),str(self.region) + str(self.number).zfill(3) + "_BIN_" + str(a-self.start).zfill(digits),str(self.Heatmap[i][a-self.start]).strip(" ")])
               b = b + 1


      def save(self,fileName,fileName2,fileName3):

              #print("testing whether to save file")
              #if (float(len(np.diag(self.Heatmap)[[np.diag(self.Heatmap)==0]])) / (float(len(self.Heatmap)))) < 0.5: # similar to deadzone
              print("saving files")
              np.savetxt("input/" + fileName, self.Heatmap, delimiter=',',fmt='%f')

              f = open("input/" + fileName2,"w") # pCount
              f.write('\n'.join('\t'.join('{:1}'.format(item) for item in row) for row in self.pcount))
              f.write('\n')
              f.close();

              f = open("input/" + fileName3,"w") # Bed
              f.write('\n'.join('\t'.join('{:1}'.format(item) for item in row) for row in self.list_of_regions))
              f.write('\n')
              f.close();
  



def process5(element):
     #looks for non-qnormed counts

     chr = element[0]
     bed = element[1]
     counts = element[2]
     sample = element[3]
     overlap = int(element[4])
     stride = int(element[5])
     logged = element[6]
     size = int(element[7])
     copies = int(np.floor(size // stride))
  
     name='HiC'+ str(chr) # region name shared across replicates, appended to bin inside output counts and bed file
     file=str(chr) + '_' + str(sample) # Update 'NAME' to be incorporated into output file name
     counts_file='input/' + str(chr) + '_' + counts #Update 'COUNTS' to the file path of specific chromosome counts file
     bed_file='input/' + str(chr) + "_" + bed #Update 'BED' to the .bed file path of specific bed counts file     

     begin = 1 
     last = size
     end = True
     start = 1 
     stop = start + stride
     rep = 1 
     if overlap > stride:
          begin = 1 + overlap
          start = 1 + overlap
          stop = start + stride
          copies = int(np.floor((size-overlap) // (stride+1)))

     final_start = size-((stride+overlap)+1)
     final_end = size-1 #final bin value 

     for i in range(0,copies):
          data = MatrixMake(counts_file,bed_file,start,stop,begin,last,rep,name,logged,stride,overlap)
          data.save(file + str(rep).zfill(5) + "_heatmap.csv",file + str(rep).zfill(5) + "_pvalues.counts",file + str(rep).zfill(5) +".bed")
          start = stop - overlap
          stop = stop + stride
          rep = rep+1
          #del data

     data = MatrixMake(counts_file,bed_file,final_start,final_end,begin,last,rep,name,logged,stride,overlap)
     data.save(file + str(rep).zfill(5) + "_heatmap.csv",file + str(rep).zfill(5) + "_pvalues.counts",file + str(rep).zfill(5) +".bed")

if __name__ == "__main__":
     process5()
