import sys
from threednetmod.read_settings import read_settings
from threednetmod.create_tags import create_tags

def result_csv_to_txt(in_file, out_file, q_value_thresh):
	input = open(in_file, 'r')
	output = open(out_file, 'w')

	for line in input:
		pieces = line.strip().split(',')
		if pieces[0].startswith('chr'):
			chr = pieces[0]
			coord = pieces[1]
			qvalue = float(pieces[-1])  # assumes that right tail qvalue is the last column!
			if qvalue< q_value_thresh:
				temp = chr + '\t' + coord
				print 'printing to output'
				print >> output, temp

	output.close()
	input.close()
	

def main():
	q_value = 0.05
	settings = read_settings(sys.argv[1])
	tags = create_tags(sys.argv[1])
	tags_HSVM = tags[3]
	tags_DBR = tags_HSVM + '_' + settings['qnorm_thresh'] + '_' + settings['boundary_size'] + '_' + str(int(settings['IS_min_dist'])/1000) + 'kb' + '_' + str(int(settings['IS_max_dist'])/1000) + 'kb'


	category_name = settings['sample_3'] +   '_specific_only_'
	outdir = 'output/DBR/'
	test_direction1 = settings['sample_3'][:-1] + '_v_' + settings['sample_1'][:-1]
	test_direction2 = settings['sample_1'][:-1] + '_v_' + settings['sample_3'][:-1]
	celltype_1_file = outdir + test_direction1 + '_' + category_name + '_' + tags_DBR + '.csv'
	print 'celltype_1_file: ', celltype_1_file
	celltype_2_file = outdir + test_direction2 + '_' + category_name + '_' + tags_DBR + '.csv'
	print 'celltype_2_file: ', celltype_2_file
	#WT_file = 'input/UPDATE_FINAL_output_option2A_WT_update_peaksDI100_40kb_440kb_within40_need0.75/boundaries_by_l2fc_is_q-value_KO specific only.csv'
	#KO_file = 'input/UPDATE_FINAL_output_option2A_KO_update_peaksDI100_40kb_440kb_within40_need0.75/boundaries_by_l2fc_is_q-value_KO specific only.csv'
	result_csv_to_txt(celltype_1_file, 'output/DBR/sig_boundaries_' + test_direction1 + '_' + category_name + '_' + settings['sample_3'] + 'spec_CTCF.txt', q_value)
	result_csv_to_txt(celltype_2_file, 'output/DBR/sig_boundaries_' + test_direction2 + '_' + category_name + '_' + settings['sample_3'] + 'spec_CTCF.txt', q_value)


if __name__ == '__main__':

	main()
