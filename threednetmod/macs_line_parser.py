def macs_line_parser(line):
    # split line on tab
    pieces = line.split('\t')

    # parse columns
    return {
        'chrom'          : pieces[0].strip(),
        'start'          : int(pieces[1]),
        'end'            : int(pieces[2]),
        'length'         : int(pieces[3]),
        'abs_summit'     : int(pieces[4]),
        'pileup'         : int(pieces[5]),
        'fold_enrichment': float(pieces[7]),
        'name'           : pieces[9].strip()
    }
