from __future__ import division, absolute_import, print_function
from threednetmod.load_primermap_qnorm import load_primermap
from threednetmod.load_counts_qnorm import load_counts
from threednetmod.qnorm_counts_superdict import qnorm_counts_superdict
from threednetmod.write_counts_qnorm import write_counts
from threednetmod.read_settings import read_settings
import glob
import sys 
import os


def process3(element):

    chr = element[0]
    bed = element[1]
    counts_1 = element[2]
    sample_1 = element[3]
    counts_2 = element[4]
    sample_2 = element[5]
    counts_3 = element[6]
    sample_3 = element[7]
    counts_4 = element[8]
    sample_4 = element[9]
    tag = element[10]

    print('loading data')
    primermap = load_primermap('input/' + chr + '_' + tag + '_' + bed)
    counts_superdict = {
        sample_1 + chr + '_' + tag: load_counts('input/' + sample_1 + chr + '_' + tag + '_primer_primer_pair_removed.counts', primermap),
        sample_2 + chr + '_' + tag: load_counts('input/' + sample_2 + chr + '_' + tag + '_primer_primer_pair_removed.counts', primermap),
        sample_3 + chr + '_' + tag: load_counts('input/' + sample_3 + chr + '_' + tag + '_primer_primer_pair_removed.counts', primermap),
        sample_4 + chr + '_' + tag: load_counts('input/' + sample_4 + chr + '_' + tag + '_primer_primer_pair_removed.counts', primermap),
    }
    print('performing quantile normalization')
    qnormed_counts_superdict = qnorm_counts_superdict(counts_superdict,
                                                      primermap)
    print('writing output')
    for rep in counts_superdict.keys():
        write_counts(qnormed_counts_superdict[rep], 'input/%s_qnormed.counts' % rep,
                     primermap)

    del qnormed_counts_superdict

def main():

     settings = read_settings(sys.argv[1]) # read in user specified settings file
     bed = settings['bed_file']
     overlap = settings['overlap']
     stride = settings['stride']
     logged = settings['logged']

     counts_1 = settings['counts_file_1']
     counts_2 = settings['counts_file_2']
     counts_3 = settings['counts_file_3']
     counts_4 = settings['counts_file_4']

     sample_1 = settings['sample_1']
     sample_2 = settings['sample_2']
     sample_3 = settings['sample_3']
     sample_4 = settings['sample_4']

     elements = []
     chromosomes = []
     elements2 = []

     if settings['scale'] == 'genomewide':
           for lines in chr_counts:
               pre = lines.strip().split('/')
               ext = pre[1].split('_')
               chr = ext[0]  #chr in prefix separated by "_"
               if chr not in chromosomes:
                   elements.append((chr,bed,counts_1,sample_1,counts_2,sample_2,counts_3,sample_3,counts_4,sample_4))
                   chromosomes.append(chr)
     else:
          print "found ", settings['scale']
          chromosomes.append(settings['scale'])
          elements.append((settings['scale'],bed,counts_1,sample_1,counts_2,sample_2,counts_3,sample_3,counts_4,sample_4))

     for i in range(0,len(elements)):
          process3(elements[i])

if __name__=="__main__":
    main()
