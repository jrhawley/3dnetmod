from __future__ import division, absolute_import, print_function
import numpy as np
from threednetmod.read_settings import read_settings

def parallelchr(element):

     #settings = read_settings('settings_genomewide.txt')
     #counts = open('input/' + settings["counts_file_1"], 'r')
     tag = element[0][4]
     counts = open('input/' + str(element[0][3]), 'r')
     #counts_out = open('input/' + element[0] + '_' + str(settings["counts_file_1"]), 'w')
     counts_out = open('input/' + element[0][0] + '_' + tag + '_' + str(element[0][3]), 'w')
     lookat = 0
     for line in counts:
          pieces = line.strip().split('\t')
          bin_1 = pieces[0]
          bin_2 = pieces[1]
          value = pieces[2]
          #if int(bin_1) >= element[1] and int(bin_1) <= element[2]:
          if int(bin_1) <= element[lookat][2]:
               modbin_1 = (int(bin_1) - element[lookat][1]) + 1 #adjust bin 1 to start at 1 per region
               modbin_2 = (int(bin_2) - element[lookat][1]) + 1 #adjust bin 2 relative to bin 1 
               temp = "%s\t%s\t%f" % (modbin_1, modbin_2, float(value))
               print(temp, file=counts_out)
          else:
               lookat = lookat + 1
               counts_out.close()
               if lookat < len(element):
                    counts_out = open('input/' + element[lookat][0] + '_' + tag + '_' + str(element[lookat][3]), 'w')
               
          #if int(line.strip().split('\t')[0]) > element[2]:
          #     break
     return 0


if __name__ == "__main__":
     parallelchr()
