def load_domain_boundaries(boundary_file):
    boundaries = {}
    with open(boundary_file, 'r') as handle:
        for line in handle:
            pieces = line.split('\t')
            chrom = pieces[0].strip()
            coord = int(pieces[1])
            if chrom not in boundaries:
                boundaries[chrom] = []
            boundaries[chrom].append(coord)
    return boundaries
