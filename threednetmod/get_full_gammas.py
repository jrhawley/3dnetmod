import numpy as np

def get_full_gammas(settings):
        gamma_start = float(settings['gamma_start'])
        gamma_stop = float(settings['gamma_stop'])
        gamma_step = float(settings['gamma_step'])
        gammas = np.arange(gamma_start, gamma_stop + gamma_step, gamma_step)
        return gammas,gamma_start,gamma_step
