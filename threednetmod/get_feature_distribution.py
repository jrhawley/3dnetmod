

def get_feature_distribution(community_list, feature):
        if feature == 'gamma':
                print 'FEATURE IS GAMMA!'
                gammas = []
                for community in community_list:
                        print community
                        gammas.append(float(community['level']))
                return gammas
        elif feature == 'size':
                sizes = []
                for community in community_list:
                        sizes.append(int(community['end']) - int(community['start']))
                return sizes

        elif feature == 'variance':
                variance = []
                for community in community_list:
                        variance.append(max(float(community['left_var']), float(community['right_var'])))
                return variance
