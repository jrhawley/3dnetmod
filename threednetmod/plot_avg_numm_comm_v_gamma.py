import sys
from threednetmod.make_scatterplots import make_scatterplots
from threednetmod.make_scatterplots import make_scatterplots_zoom

def main():
	avg_num_comm_file = sys.argv[1]
	input = open(avg_num_comm_file, 'r')
	x = []
	y = []
	for line in input:
		if line.startswith('#'):
			continue
		else:
			pieces = line.strip().split('\t')
			x.append(float(pieces[0]))
			y.append(float(pieces[1]))
	filename = avg_num_comm_file.split('/')[-1] + 'full_scatter_plot.png'
	dir = 'output/Avg_num_comm_scatterplots/'
	make_scatterplots(x, y, filename, dir)


	filename = avg_num_comm_file.split('/')[-1] + 'zoom_scatter_plot.png'
	make_scatterplots_zoom(x, y, filename, dir, (1.2,1.6))


if __name__ == '__main__':
	main()
