from threednetmod.unflatten_counts_single_region_geometric import *

def unflatten_counts_all_regions(flattened_counts_list, region_order, pixelmap):
    """
    Unflattens a single list of flattened counts from many regions into a
    standard counts dict structure.

    Parameters
    ----------
    flattened_counts_list : list of float
        The list of flattened counts to be unflattened. See
        ``lib5c.util.counts.flatten_counts_to_list()``.
    region_order : list of str
        The list of region names in the order that the regions were concatenated
        in when making the ``flattened_counts_list`` See
        ``lib5c.util.counts.flatten_counts_to_list()``.
    pixelmap : dict of list of dict
        A pixelmap or primermap. This will be used to determine the size of each
        region. See ``lib5c.parsers.primers.get_pixelmap()`` or
        ``lib5c.parsers.primers.get_primermap()``.

    Returns
    -------
    dict of 2d numpy arrays
        The keys are the region names. The values are the arrays of counts
        values for that region. These arrays are square and symmetric.
    """
    # the dict we will put the unflattened counts into
    counts = {}

    # keeps track of where we are in the flattened_counts_list
    # counts with indices lower than this number have already been unflattened
    # into counts
    index_pointer = 0

    for region in region_order:
        # get the size of this region
        region_size = len(pixelmap[region])

        # calculate the length of a flattened_regional_counts list for a region
        # of this size
        flattened_size = region_size*(region_size+1) / 2

        # assume that the next region_size entries in the flattened_counts_list
        # belong to this region
        counts[region] = unflatten_counts_single_region(
            flattened_counts_list[index_pointer:index_pointer + flattened_size])

        # increment the index_pointer
        index_pointer += flattened_size

    return counts
