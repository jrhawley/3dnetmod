def dblalt_primer_name_parser(name):
    """
    The double alternating primer name parser.

    Parameters
    ---------
    name : str
        The name of the primer found in the appropriate column of the primer
        bedfile.

    Returns
    -------
    dict
        This dict has the following structure::

            {
                'region': str,
                'number': int,
                'orientation': "3'" or "5'",
                'name': str
            }

        These fields are parsed from the primer name.

    Notes
    -----
    You can write other name parsers to accommodate different primer naming
    conventions.
    """
    pieces = name.split('_')
    region = pieces[2].split('-')[0]
    if len(pieces[4].split(':')[0].split('|')) == 1:
        raise ValueError('dblalt primer name scheme violation')
    number = int(pieces[4].split(':')[0].split('|')[0])
    if pieces[3].split('-')[0] in ['FOR', 'LFOR']:
        orientation = "3'"
    elif pieces[3].split('-')[0] in ['REV', 'LREV']:
        orientation = "5'"
    else:
        raise ValueError('dblalt primer name scheme violation')
    corrected_name = name.split(':')[0]+':'+name.split(':')[1]

    return {'region'     : region,
            'number'     : number,
            'orientation': orientation,
            'name'       : corrected_name}
