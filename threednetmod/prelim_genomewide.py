#from slice_genomic_coordinates_deadzones import parallel
import numpy as np
from split_counts_by_chromosome import process
import multiprocessing
from threednetmod.read_settings import read_settings
from threednetmod.slice_genomic_coordinates_genomewide_HiC import process5
#import fileinput
import os
import subprocess
import sys 
import glob

'''
   Heidi Norton
   Daniel Emerson
   Harvey Huang


   prelim_genomewide.py reads settings from settings_genomewide.txt.

   If running locally keep line: pool = multiprocessing.Pool()
   This will automatically set to number of cores available for multiprocessing

   If on cluster, multiprocessing.Pool() can be adjusted to number of threads desired
        pool = multiprocessing.Pool(#_of_threads)

   Also, adjust line  pool = multiprocessing.Pool() in split_counts_by_chromosome.py
   to include specific thread number if desired when on cluster

   Script runs in 3 phases:
         1. splices counts and bed in input directory by chromosome number.  This
            dramatically reduces the time for determining bed and counts per region
            per chr for phase 2.  (only have to read and process by chr for phase 2) 
            Uses multiprocessing in split_counts_by_chromosome.py to expedite time
            in splitting by chromosome 
         2. run slice_genomic_coordinates.py.  Uses as input the bed and counts
            outputted in phase 1. Uses the stride, overlap to determine counts
            and bed per region per chromosome. next starts at end of previous minus overlap.
            Output counts are either logged or not (set by user in settings*.txt)
            Uses multiprocessing from this script to expedite time in determining counts and
            bed per region.
         3. Concatenates counts and bed per region per chr. Appends final.bed or
            finalpvalues.counts per chr in input directory and removes all metafiles created
            up until final concatenated region counts and beds per chr.  After final bed and
            counts, ready to use main_genomewide_partition_avg_num_comm.py  

   Can run multiple counts for multiple celltypes if they correspond to same bed file
                 
 '''


def main():

     settings = read_settings(sys.argv[1]) # read in user specified settings file
     bed = settings['bed_file']
     #counts = settings["counts_file_1"]
     overlap = settings['overlap']
     stride = settings['stride']
     logged = settings['logged']
     #sample = settings["sample_1"]

     counts = []
     for counts_inst in settings:
          if counts_inst[:12] == 'counts_file_':
               counts.append(settings[counts_inst])

     samples= []
     for sample_inst in settings:
         if sample_inst[:7] == 'sample_':
             samples.append(settings[sample_inst])

     #find all chromosomes and end bin values based on one of celltype counts and bed
     for i in range(len(counts)): #iterate through all celltypes with corresponding counts 
          chr,ends = process(bed,counts[i],int(settings['processors']))  #phase 1

     settings_repeat = []
     for i in range(0,len(chr)):
          settings_repeat.append(settings)     

     print "chr = ",chr
     print "end = ",ends
    
     complete = []
     for i in range(len(samples)): #iterate through all celltypes with corresponding counts
          for j in range(0,len(chr)):
               complete.append((chr[j],bed,counts[i],samples[i],overlap,stride,logged,ends[j]))

     
     os.system("taskset -p 0xff %d" % os.getpid())
     pool = multiprocessing.Pool(int(settings['processors'])) #uses default number of CPUs available locally (may change if cluster is available)
     results = pool.map(process5, complete) 
     
     #phase 3
     
     for sample_inst in samples:
          for chromosome in chr:
               input_list = 'input/' + str(chromosome) + '_' + sample_inst + '0*.bed' #assume < 100 regions per chromosome
               output_name = 'input/' + str(chromosome) + '_' + sample_inst  + 'final.bed'
               command = 'cat '+ input_list + ' > ' + output_name
               #if os.path.exists('input/' + str(chromosome) + '_' + settings["sample_1"] + '001.bed'):
               if len(glob.glob('input/' + str(chromosome) + '_' + sample_inst + '0*.bed')) > 0:  #ensure files exist before cat
                    os.system(command)

                    remove_bed = 'input/' + str(chromosome) + '_' + sample_inst + '0*.bed'
                    command_removal = 'rm ' +  remove_bed
                    os.system(command_removal)

               input_list = 'input/' + str(chromosome) + '_' + sample_inst + '0*pvalues.counts'
               output_name = 'input/' + str(chromosome) + '_' + sample_inst + 'finalpvalues.counts' 
               command = 'cat '+ input_list + ' > ' + output_name
               #if os.path.exists('input/' + str(chromosome) + '_' + settings["sample_1"] + '001_pvalues.counts'):
               if len(glob.glob('input/' + str(chromosome) + '_' + sample_inst + '0*pvalues.counts')) > 0: #ensure files exist
                    os.system(command)
 
                    remove_counts = 'input/' + str(chromosome) + '_' + sample_inst + '0*pvalues.counts' 
                    command_removal = 'rm ' + remove_counts
                    os.system(command_removal)

                    remove_heatmap = 'input/' + str(chromosome) + '_' + sample_inst +  '*_heatmap.csv'
                    command_removal = 'rm ' + remove_heatmap
                    os.system(command_removal)

               remove_splice_bed = 'input/' + str(chromosome) + '_' + settings["bed_file"]
               command_removal = 'rm ' + remove_splice_bed
               os.system(command_removal)

               remove_splice_counts = 'input/' + str(chromosome) + '_' + sample_inst
               command_removal = 'rm ' + remove_splice_counts
               os.system(command_removal)
     

if __name__ == "__main__":
     main()
