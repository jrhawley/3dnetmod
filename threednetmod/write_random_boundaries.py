from threednetmod.load_primermap import load_primermap
from threednetmod.write_domain_boundaries import write_domain_boundaries
import random


def write_random_boundaries(settings):

    num_positions = 5000

    # data structure to store selections
    selected_positions = {}

    # load and reverse primermap
    primermap = load_primermap('input/' + settings['bed_file'])
    reverse_primermap = {primer['name']: primer
                         for chrom in primermap
                         if chrom not in ['chrM', 'chrY']
                         for primer in primermap[chrom]
                         if primer['start'] > 3000000}

    # draw random positions from the reverse primermap
    for i in range(num_positions):
        random_index = random.randint(0, len(reverse_primermap) - 1)
        random_key = reverse_primermap.keys()[random_index]
        primer = reverse_primermap[random_key]
        if primer['chrom'] not in selected_positions:
            selected_positions[primer['chrom']] = []
        selected_positions[primer['chrom']].append(primer['start'])

    # sort selections
    for chrom in selected_positions:
        selected_positions[chrom].sort()

    # write to disk
    write_domain_boundaries(
        selected_positions, 'input/random_%i.txt' % num_positions)
