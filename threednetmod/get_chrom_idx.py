

def get_chrom_idx(boundaries_path, bin_size):
        chrom_idx = {}
        input = open(boundaries_path, 'r')
        chroms = []
        chrom_coord = {}
        for line in input:
                if line.startswith('#'):
                        continue
                else:
                        pieces = line.strip().split('\t')
                        chrom = pieces[0]
                        if chrom not in chroms:
                                chroms.append(chrom)
                                chrom_coord[chrom] = []
                        chrom_coord[chrom].append(int(pieces[1]))


        for chrom in chrom_coord:
                chrom_idx[chrom] = {}
                chrom_idx[chrom]['start_bin'] = min(chrom_coord[chrom]) // bin_size
                chrom_idx[chrom]['stop_bin'] = max(chrom_coord[chrom]) // bin_size


        print 'chrom_idx: ', chrom_idx


        return chrom_idx
