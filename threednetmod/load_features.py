# -*- coding: utf-8 -*-
"""
Module for parsing .bed files.
"""

from threednetmod.guess_field_parser_by_looping import guess_field_parser
from threednetmod.count_intersections import count_intersections


def load_features(filename, field_parser=None, boundaries=None):
    """
    Loads BED-formatted features from a file into dict of lists organized by
    chromosome.

    Parameters
    ----------
    filename : str
        The filename to read BED features from.
    field_parser : Optional[Callable[[str], Dict[str, Any]]]
        A function that takes in a line in the BED file and returns a dict
        representing the feature described by that line. The dict must have at
        least the following structure::

            {
                'chrom': str,
                'start': int,
                'end'  : int
            }

        Pass None to let the function guess an appropriate column scheme.
    boundaries : Optional[List[Dict[str, Any]]]
        If passed, features will only be loaded if they intersect at least one
        of the features in this list. The features should be represented as
        dicts with the following structure::

            {
                'chrom': str,
                'start': int,
                'end'  : int
            }

    Returns
    -------
    Dict[str, List[Dict[str, Any]]]
        The keys to the outer dict are chromosome names as strings. The elements
        of the list are dicts representing features, containing the information
        found in the columns of the source file.
    """
    # dict to store parsed data
    # keys will be chromosome names as strings, values will be sorted lists of
    # the features parsed on that chromosome
    feature_data = {}

    with open(filename, 'r') as handle:
        for line in handle:
            # skip comments and track line
            if line.startswith('#') or line.startswith('track'):
                continue
            # guess a field parser if one wasn't provided
            if field_parser is None:
                field_parser = guess_field_parser(line)

            # parse the line
            line_info = field_parser(line)

            # if we haven't seen this chrom before, make a new entry in the
            # feature_data dict
            if line_info['chrom'] not in feature_data:
                feature_data[line_info['chrom']] = []

            # add this line's information to the feature_data dict
            if (boundaries is None) or \
                    (count_intersections(line_info, boundaries) > 0):
                feature_data[line_info['chrom']].append(line_info)

    # make sure everything is sorted
    for chrom in feature_data:
        feature_data[chrom].sort(key=lambda x: x['start'])

    return feature_data
