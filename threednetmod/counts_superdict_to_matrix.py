import numpy as np
from threednetmod.flatten_counts_all_regions_geometric import *


def counts_superdict_to_matrix(counts_superdict, rep_order=None,
                               region_order=None, discard_nan=False):
    """
    Convert a counts_superdict structure to a matrix.

    Parameters
    ----------
    counts_superdict : Dict[Dict[np.ndarray]]
        The keys of the outer dict are replicate names, the keys of the inner
        dict are region names, the values are square symmetric arrays of counts
        for the specified replicate and region.
    rep_order : Optional[List[str]]
        The order in which the replicates should be arranged. Pass None to use
        the order of ``counts_superdict.keys()``.
    region_order : Optional[List[str]]
        The order in which the regions should be concatenated. Pass None to use
        the order of the keys.
    discard_nan : bool
        If True, positions containing ``nan`` values will be removed.

    Returns
    -------
    np.ndarray
        Each row is a replicate, each column is a position. The order of the
        columns is described in ``lib5c.util.counts.flatten_counts_to_list()``,
        honoring the passed region order.

    Examples
    --------
    >>> counts_superdict = {'Rep1': {'A': np.array([[1, 2], [2, 3]]),
    ...                              'B': np.array([[4, 8], [8, 10]])},
    ...                     'Rep2': {'A': np.array([[3, 5], [5, 6]]),
    ...                              'B': np.array([[7, 9], [9, np.nan]])}}
    >>> rep_order = ['Rep1', 'Rep2']
    >>> region_order = ['A', 'B']
    >>> counts_superdict_to_matrix(counts_superdict, rep_order, region_order)
    array([[  1.,   2.,   3.,   4.,   8.,  10.],
           [  3.,   5.,   6.,   7.,   9.,  nan]])
    >>> counts_superdict_to_matrix(counts_superdict, rep_order, region_order,
    ...                            discard_nan=True)
    array([[ 1.,  2.,  3.,  4.,  8.],
           [ 3.,  5.,  6.,  7.,  9.]])
    """
    # default rep_order
    if rep_order is None:
        rep_order = counts_superdict.keys()

    # default region_order
    if region_order is None:
        region_order = counts_superdict[rep_order[0]].keys()

    # create matrix
    matrix = np.array([flatten_counts_all_regions(counts_superdict[rep],
                                              region_order=region_order,
                                              discard_nan=False)[1]
                       for rep in rep_order])

    # discard nans
    if discard_nan:
        matrix = matrix[:, ~np.isnan(matrix).any(axis=0)]

    return matrix

