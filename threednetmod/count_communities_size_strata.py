import sys
from threednetmod.read_settings import read_settings
from threednetmod.create_tags import create_tags
import glob
from threednetmod.load_community_list_DBR import load_community_list

"""
Heidi Norton
"""

def main():
	settings = read_settings(sys.argv[1])
	chr = sys.argv[2]
	tags = create_tags(sys.argv[1])
	tags_DBR = tags[3]
	tags_HSVM = tags[2]
	sizes = []
	size_strata = []    
	for size_inst in settings:
		if size_inst[:6]=='size_s':
			sizes.append(int(settings[size_inst]))
	sizes = sorted(sizes)

	previous_size = 0
	for i in range(len(sizes)):
		size = sizes[i]
		stratum = (previous_size, size)
		size_strata.append(stratum)
		previous_size = size


	if settings['merge'] == 'True':
		dir = 'output/unique_communities_thresholded_post/results_files/merged/'
	else:
		dir = 'output/unique_communities_thresholded_post/results_files/size_stratified/'
	fname = dir + '*' + chr + '*' 
	community_files = glob.glob(fname)
	strata_count = {}
	for file in community_files:
		print file
		for stratum in size_strata:
			strata_count[stratum] = 0
		communities = load_community_list(file)
		for community in communities:
			if settings['merge'] == 'True':
				size = int(community['end2']) - int(community['start1'])
			else:
				size = int(community['end']) - int(community['start'])
			for stratum in size_strata:
				if size > stratum[0] and size <= stratum[1]:
					strata_count[stratum] +=1
		for stratum in size_strata:
			print stratum
			print 'number of communities: ', strata_count[stratum]


         
         

               

        



if __name__ == '__main__':
        main()
