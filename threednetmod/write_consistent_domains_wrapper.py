import sys
from threednetmod.read_settings import read_settings
from threednetmod.create_tags import create_tags
#from dynamic_boundary_reconfiguration import DBR
#from dynamic_boundary_reconfiguration_match_boundaries import DBR
#from dynamic_boundary_reconfiguration import DBR
#from classify_boundaries_par_size_stratified import classify_boundaries
from threednetmod.write_classified_boundaries import write_classified_boundaries
from threednetmod.plot_histogram import plot_histogram
import csv
import operator
from threednetmod.dynamic_boundary_reconfiguration_match_communities_update_consistent import DBR_communities
from threednetmod.write_consistent_communities import write_consistent_communities



"""
Heidi Norton
"""

def main():
    settings = read_settings(sys.argv[1])
    tags = create_tags(sys.argv[1])
    tags_DBR = tags[4]
    tags_preprocessing = tags[0]
    tags_HSVM = tags[3]
    write_consistent_communities(settings, tags_preprocessing, tags_HSVM)

              

if __name__ == '__main__':
        main()
