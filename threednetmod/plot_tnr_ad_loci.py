#from threednetmod.load_binned_counts import load_binned_counts
from threednetmod.load_communities import load_communities
#from threednetmod.load_pixelmap import load_pixelmap
#from threednetmod.plot_heatmap import plot_heatmap
from threednetmod.plot_heatmap_two_community_sets_colored import plot_heatmap
from threednetmod.get_colormap import get_colormap
import numpy as np
from threednetmod.flatten_counts_single_region_geometric import flatten_counts_single_region
#from threednetmod.parse_community_calls_by_region import parse_community_calls_by_region
from threednetmod.parse_community_calls_by_region import parse_netmod_community_calls_by_region
from threednetmod.load_binned_counts import load_binned_counts
import sys
import os
from threednetmod.read_settings import read_settings
from threednetmod.counts_to_array import counts_to_array
from threednetmod.load_pixelmap import load_pixelmap
from threednetmod.load_region_start_end_coord import load_region_start_end_coord

def get_zoom_region(zoom_coord,  region_coords):
        print 'zoom_coord: ', zoom_coord
        for region in region_coords:
                if zoom_coord['chrom'] == region_coords[region]['chrom']:
                        if region_coords[region]['start'] < zoom_coord['start'] and region_coords[region]['end'] > zoom_coord['end']:
                                print 'found region: ', region
                                return region



def main():


                zoom_dict = {}
                zoom_dict['chrom'] = chr
                zoom_dict['start'] = community['start']
                zoom_dict['end'] = community['stop']
                zoom_region = get_zoom_region(zoom_dict, region_coords)



	region_coords, bin_size  = load_region_start_end_coord(bed_filename)
	settings = read_settings(sys.argv[1])
	sample = settings['sample_1']
	cell_type_input = 'chr5' + '_' + sample
	cell_type = 'chr5' + sample
	counts_file = 'input/' +  cell_type_input + "finalpvalues.counts"
	bed_filename = 'input/' + cell_type_input + "final.bed"
	counts,_ = counts_to_array(counts_file,bed_filename)
	pixelmap = load_pixelmap(bed_filename)

	for region in ['HiCchr5037']:	
	#for region in ['HiCchr6005']:
	#for region in ['HiCchr12029']:
	#for region in ['HiCchr14022']:
	#for region in ['HiCchr19003']:
	#for region in ['HiCchr3015']:
	#for region in ['HiCchr6039']: #does not work - badregion
	#for region in ['HiCchrX019']: #does not work - badregion
	#for region in ['HiCchr12001']: #not far enough left
	#for region in ['HiCchr9019']:
	#for region in ['HiCchr3032']:
	#for region in ['HiCchr22010']: 
	#for region in ['HiCchr16016']:
	#for region in ['HiCchr9006']:
	#for region in ['HiCchr19003']:
	#for region in ['HiCchr2044']:
	#for region in ['HiCchr7006']:
	#for region in ['HiCchr6011']:
	#for region in ['HiCchr13026']:
	#for region in ['HiCchr14007']: #not far enough left
	#for region in ['HiCchr3033']:
	#for region in ['HiCchrX006']:
	#for region in ['HiCchr19011']:
	#for region in ['HiCchr13017']:
	#for region in ['HiCchr16020']:
	#for region in ['HiCchr21010']:
		start_coord = pixelmap[region][0]['start']
		end_coord = pixelmap[region][-1]['end']
		chr = pixelmap[region][0]['chrom']
		colorscale=(0.0,np.percentile(flatten_counts_single_region(counts[region], discard_nan=True),98))
		if settings['merge'] == 'True':
			community_file = 'output/unique_communities_thresholded_post/results_files/merged/Merged_' + 'Communities_' + settings['num_part'] + '_partitions_' +settings['pctile_threshold'] + '_' + settings['pct_value'] + '_trash_community_removal_' + settings['diagonal_density'] + '_' + settings['consecutive_diagonal_zero'] + '_bad_regions_' + cell_type + '_gammastep_' + settings['gamma_step'] + '_hierarchy_' + settings['hierarchies'] + settings['gamma_part'] + '_gammapart_' + settings['plateau'] + '_plateau_thresholded_post_writing_' + settings['var_thresh1'] + '_' + str(settings['var_thresh2']) + '_' + str(settings['var_thresh3']) + '_' + str(settings['var_thresh4']) + '_' + str(settings['var_thresh5']) + '.txt'
			dir = 'output/heatmaps_w_communities/merged/good_regions/'				
			if not os.path.isdir(dir): os.makedirs(dir)
		else:
			community_file = 'output/unique_communities_thresholded_post/results_files/size_stratified/Communities_' + settings['num_part'] + '_partitions_' +settings['pctile_threshold'] + '_' + settings['pct_value'] + '_trash_community_removal_' + settings['diagonal_density'] + '_' + settings['consecutive_diagonal_zero'] + '_bad_regions_' + cell_type  + '_gammastep_' + settings['gamma_step'] + '_hierarchy_' + settings['hierarchies'] + settings['gamma_part'] + '_gammapart_' + settings['plateau'] + '_plateau_thresholded_post_writing_' + settings['var_thresh1'] + '_' + str(settings['var_thresh2']) + '_' + str(settings['var_thresh3']) + '_' + str(settings['var_thresh4']) + '_' + str(settings['var_thresh5']) + '.txt'
			dir = 'output/heatmaps_w_communities/unmerged/good_regions/'
			if not os.path.isdir(dir): os.makedirs(dir)

		communities = parse_netmod_community_calls_by_region(community_file, chr, start_coord, end_coord)  
		#communities = load_communities(community_file)
		#print "communities = ",communities

		heatmap_filename =  dir + cell_type_input + '_' + region + 'chr5_1v4.png'
		bed_filename2 = 'input/2016-12-28_hg19_refseq_sorted.bed'
		coloredgenes = 'input/coloredgenes.txt'
		plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr5:110760000-147758348', 'chr5:110760000-147758348'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr6:14799342-18261721', 'chr6:14799342-18261721'),colorscale = colorscale, show_colorscale = True, cmap	 = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr12:110390017-113537480', 'chr12:110390017-113537480'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr14:91024895-94072965', 'chr14:91024895-94072965'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr19:11817255-15117274', 'chr19:11817255-15117274'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr3:62350232-65489136', 'chr3:62350232-65489136'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr6:169363420-172381958', 'chr6:169363420-172381958'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)  #badregion
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chrX:65263873-68450461', 'chrX:65263873-68450461'),colorscale = colorscale, show_colorscale = True, cmap	 = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)  #badregion
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr12:5533625-8551484', 'chr12:5533625-8551484'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2) #does not extend far enough to left
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr9:70150478-73193993', 'chr9:70150478-73193993'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr3:127386657-130402810', 'chr3:127386657-130402810'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr22:44567677-47741187', 'chr22:44567677-47741187'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr16:64961199-68016745', 'chr16:64961199-68016745'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr9:26046543-29073491', 'chr9:26046543-29073491'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr19:17393582-20402114', 'chr19:17393582-20402114'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr2:175457531-178460666', 'chr2:175457531-178460666'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr7:25736498-28739725', 'chr7:25736498-28739725'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr6:43796053-47018819', 'chr6:43796053-47018819'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr13:99134025-102139019', 'chr13:99134025-102139019'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr14:22289396-25295394', 'chr14:22289396-25295394'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2) #not far enough left
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr3:137163065-140165982', 'chr3:137163065-140165982'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chrX:23521812-26534065', 'chrX:23521812-26534065'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr19:44772966-47783861', 'chr19:44772966-47783861'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr13:69181344-72213885', 'chr13:69181344-72213885'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr16:86136492-89138254', 'chr16:86136492-89138254'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)
		#plot_heatmap(counts[region], heatmap_filename, zoom_window=('chr21:43693545-46696256', 'chr21:43693545-46696256'),colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = bed_filename2)

if __name__ == '__main__':
	main()
	
