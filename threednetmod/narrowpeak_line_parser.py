def narrowpeak_line_parser(line):
    # split line on tab
    pieces = line.split('\t')

    # parse columns
    return {
        'chrom'          : pieces[0].strip(),
        'start'          : int(pieces[1]),
        'end'            : int(pieces[2]),
        'name'           : pieces[3].strip(),
        'score'          : float(pieces[4]),
        'strand'         : pieces[5].strip(),
        'signalValue'    : float(pieces[6]),
        'pValue'         : float(pieces[7]),
        'qValue'         : float(pieces[8]),
        'peak'           : int(pieces[9]) + int(pieces[1])
                           if int(pieces[9]) > -1 else -1
    }

