from threednetmod.plot_heatmap import plot_heatmap
#from threednetmod.plot_heatmap_two_community_sets_colored import plot_heatmap
from threednetmod.get_colormap import get_colormap
import numpy as np
from threednetmod.flatten_counts_single_region_geometric import flatten_counts_single_region
from threednetmod.parse_community_calls_by_region import parse_netmod_community_calls_by_region
from threednetmod.parse_community_calls_by_region import parse_netmod_community_calls_by_region_merged
import sys
import os
from threednetmod.read_settings import read_settings
from threednetmod.counts_to_array import counts_to_array
from threednetmod.load_pixelmap import load_pixelmap
from threednetmod.load_region_start_end_coord import load_region_start_end_coord
from threednetmod.create_tags import create_tags
import glob
#from threednetmod.load_bad_regions_simplified_per_chrom import load_bad_regions
from threednetmod.load_bad_regions import load_bad_regions
from threednetmod.convert_tag_genomewide import convert_tag_genomewide

def get_zoom_region(zoom_coord,  region_coords):
	print 'zoom_coord: ', zoom_coord
	for region in region_coords:
		if zoom_coord['chrom'] == region_coords[region]['chrom']:
			if region_coords[region]['start'] < zoom_coord['start'] and region_coords[region]['end'] > zoom_coord['end']:
				print 'found region: ', region
				return region

	print 'DID NOT FIND REGION!'
	return None


def load_loci(file, window):
	input = open(file, 'r')
	loci = []
	for line in input:
		if line.startswith('#'):
			continue
		else:
			pieces = line.strip().split('\t')
			locus = {}
			locus['gene'] = pieces[3]
			locus['chrom'] = pieces[0]
			locus['start'] = int(pieces[1]) - window // 2
			locus['end'] = int(pieces[2]) + window // 2
			loci.append(locus)

	return loci



def main():


	settings = read_settings(sys.argv[1])
	tags = create_tags(sys.argv[1])
	tag_preprocess = tags[0]
	tag_GPS = tags[1]
	tag_MMCP = tags[2]
	tag_HSVM = tags[3]
	tag_DBR = tags[4]


	chr = sys.argv[2]
	#dir = 'output/heatmaps_w_communities_no_bad_regions/'
	dir = 'output/heatmaps_w_communities/good_regions/'
	dir2 = 'output/heatmaps_w_communities/bad_regions/'
	if not os.path.isdir(dir): os.makedirs(dir)
	if not os.path.isdir(dir2): os.makedirs(dir2)

	samples= []
	for sample_inst in settings:
		if sample_inst[:7] == 'sample_':
			samples.append(settings[sample_inst])
	bad_regions = load_bad_regions(settings,tag_GPS,chr)
	for cell_type in samples:
		counts_files = glob.glob('input/' + chr + '.*' + '_' + cell_type + '_' + tag_preprocess + 'finalpvalues.counts')
		for counts_file in counts_files:
			bed_filename = 'input/' + counts_file.split('/')[-1][:-14] + '.bed'
			#subchrom = input/chr18.19_WT1Neuronalchr18_100_50_True_Falsefinal.bed 
			subchrom = bed_filename.split('/')[-1].split('_')[0]
			
			region_coords, bin_size = load_region_start_end_coord(bed_filename)
			counts,_ = counts_to_array(counts_file,bed_filename)
			pixelmap = load_pixelmap(bed_filename)
			#community_file = 'output/HSVM/variance_thresholded_communities/unmerged/' + 'Communities_' + chr + cell_type + '_' + tag_HSVM + '.txt'
			community_file_merged = 'output/HSVM/variance_thresholded_communities/merged/' + 'Merged_Communities_' + chr + cell_type + '_' + tag_HSVM + '.txt'
			if not os.path.isfile(community_file_merged):
				tag_HSVM = convert_tag_genomewide(tag_HSVM, settings)
				community_file_merged = 'output/HSVM/variance_thresholded_communities/merged/' + 'Merged_Communities_' + chr + cell_type + '_' + tag_HSVM + '.txt'
                
			for region in counts:
				start_coord = pixelmap[region][0]['start']
				end_coord = pixelmap[region][-1]['end']
				colorscale=(0.0,np.percentile(flatten_counts_single_region(counts[region], discard_nan=True),98))
				#communities = parse_netmod_community_calls_by_region(community_file, chr, start_coord, end_coord)
				communities_merged = parse_netmod_community_calls_by_region_merged(community_file_merged, chr, start_coord, end_coord)
				print 'communities_merged: ', communities_merged
				print 'region: ', region
				#print 'communities: ', communities
				#if len(communities) > 0:
				if (region,subchrom) not in bad_regions:
					#heatmap_filename = dir + cell_type + '_' + subchrom + '_' + region + '_' + tag_HSVM + '.png'
					#plot_heatmap(counts[region], heatmap_filename, colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = 'input/gene_tracks/empty_track.txt')
					heatmap_filename2 = dir + cell_type + '_' + subchrom + '_' + region + '_' + tag_HSVM + '_Merged_.png'
					plot_heatmap(counts[region], heatmap_filename2, colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities_merged, primer_file = (bed_filename), gene_track = 'input/gene_tracks/empty_track.txt')
				else:
					heatmap_filename2 = dir2 + cell_type + '_' + subchrom + '_' + region + '_' + tag_HSVM + '_Merged_.png'
                                        plot_heatmap(counts[region], heatmap_filename2, colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities_merged, primer_file = (bed_filename), gene_track = 'input/gene_tracks/empty_track.txt')
					
					print 'bad regin'


if __name__ == '__main__':
	main()
	
