import os
import pandas as pd
from threednetmod.collect_is_info import collect_is_info
from threednetmod.load_primermap import load_primermap
from threednetmod.make_is_plots import make_is_plots

def compute_insulation_scores(settings, categorized_boundaries, tags_DBR, resolution, samples):
	within = resolution
	category_names = categorized_boundaries.keys()
        condition_name_map = {}
        for condition in samples:
                condition_name_map[condition] = condition
        if os.path.exists('output/DBR/data_frame_' + tags_DBR + '_' + samples[0] + '_boundaries.pickle'):
                print 'loading data frame: ' 
                df = pd.read_pickle('output/DBR/data_frame_' + tags_DBR + '_' + samples[0] + '_boundaries.pickle')
		is_window_size = ((int(settings['IS_max_dist']) - int(settings['IS_min_dist'])) / resolution) / 2
		make_is_plots(settings, df, category_names, condition_name_map, is_window_size, tags_DBR)


                return df
        primermap = load_primermap('input/' + settings['bed_file'])


        for category in categorized_boundaries:
                for chrom in primermap:
                #for chrom in ['chr1', 'chr2', 'chr3']:
                        print 'chrom: ', chrom
                        if chrom not in categorized_boundaries[category]:
                                categorized_boundaries[category][chrom] = []

	min_dist = int(settings['IS_min_dist'])
	max_dist = int(settings['IS_max_dist'])

        even = (min_dist / resolution) % 2 == 0
        if even:
                if min_dist % (resolution * 2) != 0:
                        raise ValueError('min_dist not an even multiple of bin resolution')
                diag_offset = (min_dist / resolution) / 2
        else:
                if (min_dist - resolution) % (resolution * 2) != 0:
                        raise ValueError('min_dist not an odd multiple of bin resolution')
                diag_offset = ((min_dist - resolution) / resolution) / 2
        if ((max_dist - min_dist) / resolution) % 2 != 0:
                raise ValueError('max_dist - min_dist not an even multiple of bin units')

        is_window_size = ((max_dist - min_dist) / resolution) / 2
	print 'is_window_size:'
	print is_window_size
        data_needed = 0.75

        no_offset_bins_away_from_boundary = 12 # what is the deal with this parameter?
        max_bins_away_from_boundary = no_offset_bins_away_from_boundary + diag_offset

	offsets = [is_window_size*(-3) -2, is_window_size*(-2) - 2, -1*is_window_size -2, -2, 2, is_window_size + 2, is_window_size*2 + 2, is_window_size*3 + 2]

        print '{chrom: chrom for chrom in primermap}'
        print {chrom: chrom for chrom in primermap}

        print 'samples: '
        print samples
        print 'condition_name_map'
        print condition_name_map

        df_parts_dict = {}
        for chrom in primermap:
                categorized_boundaries_per_chrom = {}
                for category in categorized_boundaries:
                        categorized_boundaries_per_chrom[category] = categorized_boundaries[category][chrom]
                        
                df_parts = collect_is_info(settings, chrom, samples, condition_name_map, categorized_boundaries_per_chrom,
                primermap[chrom], is_window_size, max_bins_away_from_boundary, diag_offset,within,data_needed, even, offsets=offsets)    
                print 'df_parts: '
                print df_parts
                df_parts_dict[chrom] = df_parts



        df = pd.concat(df_parts_dict.values(), ignore_index=True)
        #print 'concatenated df: '
        pd.to_pickle(df, 'output/DBR/data_frame_' + tags_DBR + '_' + samples[0] + '_boundaries.pickle')
	make_is_plots(settings, df, category_names, condition_name_map, is_window_size, tags_DBR)

	return df
