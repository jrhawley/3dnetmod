import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as pyplot

def plot_histogram(filename, data, bins = 160):
	results, edges = np.histogram(data, bins = bins, density = True)
	binWidth = edges[1] - edges[0]
	pyplot.bar(edges[:-1], results*binWidth, binWidth, color = '0.87')
	pyplot.savefig(filename, dpi = 300, bbox_inches = 'tight')
	pyplot.clf()
	pyplot.close()
