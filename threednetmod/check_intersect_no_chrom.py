def check_intersect(a, b):
    """
    Checks to see if two features intersect.

    Parameters
    ---------
    a, b : tuples (start, end)
        The two features to check for intersection.

    Returns
    -------
    bool
        True if the features intersect, False otherwise.


    """
    if (
            a[1] >= b[0] and
            b[1] >= a[0]
    ):
        return True
    #elif (a[0] == b[0] and a[1] == b[1]):
        #return True
    else:
        return False

