from __future__ import division, absolute_import, print_function
from threednetmod.create_bin_location_dict import create_bin_location_dict
from operator import itemgetter
from itertools import groupby
import os

def write_bad_subregions(bad_subregion_dict, bed_file, tags_GPS, sample):
	bin_location = create_bin_location_dict(bed_file)

	#bin_location[region][index_bin] = [bin_name, chr, start, end]
	chr = bed_file.split('/')[-1].split('_')[0]	
	dir = 'output/MMCP/low_structure_subregions/'
	if  not os.path.isdir(dir): os.makedirs(dir)	
	fname = dir +  chr + sample + tags_GPS + '.txt'
	output = open(fname, 'w')
	header = '#chr\tstart\tstop'
	print(header, file=output)

	for region in bad_subregion_dict:
		a = bad_subregion_dict[region]
		for k, g in groupby(enumerate(a), lambda (i, x): i - x):
			group = map(itemgetter(1), g)
			print('group: ', group)
			start_bin = min(group)
			end_bin = max(group)
			chr = bin_location[region][start_bin][1]
			start_coord = bin_location[region][start_bin][2]
			end_coord = bin_location[region][end_bin][3]

			temp = chr + '\t' + str(start_coord) + '\t' + str(end_coord)
			print(temp, file=output)


	output.close()


