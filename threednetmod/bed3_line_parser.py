def bed3_line_parser(line):
    # split line on tab
    pieces = line.split('\t')

    # parse columns
    return {
        'chrom'          : pieces[0].strip(),
        'start'          : int(pieces[1]),
        'end'            : int(pieces[2])
    }
