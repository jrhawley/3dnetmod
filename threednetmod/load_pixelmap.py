from threednetmod.default_bin_name_parser import default_bin_name_parser


def load_pixelmap(bedfile, name_parser=default_bin_name_parser):
    """
    Parameters
    ----------
    bedfile : str
        String reference to a binned primer bedfile to use to generate the
        pixelmap.
    name_parser : Optional[Callable[[str], Dict[str, Any]]]
        Function that takes in the bin name column of the bedfile (the fourth
        column) and returns a dict containing key-value pairs to be added to the
        dict that represents that bin. At a minimum, this dict must have the
        following structure::

            {
                'region': str
            }

        If the dict includes any keys that are already typically included in the
        bin dict, the values returned by this function will overwrite the usual
        values.

    Returns
    -------
    Dict[str, List[Dict[str, Any]]]
        The keys of the outer dict are region names. The values are lists, where
        the :math:`i` th entry represents the :math:`i` th bin in that region.
        Bins are represented as dicts with the following structure::

            {
                'chrom': str,
                'start': int,
                'end'  : int,
                'name' : str
            }

        Additional keys may be present if returned by ``name_parser``.

    Notes
    -----
    A pixelmap is a mapping from bins (specified by a region name and bin or
    primer index) to the genomic range covered by those bins.
    """

    # dict to store the pixelmap
    pixelmap = {}

    # parse bedfile
    with open(bedfile, 'r') as handle:
        # parse the bedfile
        for line in handle:
            # skip comments
            if line.startswith('#'):
                continue

            # split line on tab
            pieces = line.strip().split('\t')

            # parse genomic coordinate information
            chrom = pieces[0]
            start = int(pieces[1])
            end = int(pieces[2])

            # parse bin name
            name_fields = name_parser(pieces[3])
            region = name_fields['region']

            # if this is a new region, make a new list for it
            if region not in pixelmap:
                pixelmap[region] = []

            # assemble the dict describing this primer
            # always-present fields
            bin_dict = {'chrom': chrom,
                        'start': start,
                        'end'  : end,
                        'name' : pieces[3]}

            # add additional fields from parse_name
            bin_dict.update(name_fields)

            # add this region to the map
            pixelmap[region].append(bin_dict)

    # sort bins within each region
    for region in pixelmap.keys():
        pixelmap[region].sort(key=lambda x: x['start'])

    return pixelmap
