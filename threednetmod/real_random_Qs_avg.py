import numpy as np
from threednetmod.calculate_modularity import calculate_modularity
from threednetmod.assign_communities_gen_louvain import assign_sorted_communities_gen_louvain
from scipy.stats import t
from threednetmod.read_seeds import read_seeds


def real_random_Qs_avg(settings,A,NG,random_networks,random_networks_NG,gamma):
    num = int(settings['gamma_part'])
    real_Qs_dict = {}
    #Q_real, Ci = calculate_modularity(A, NG, gamma, seed)
    seed = 3443363518
    seed_count = 0
    gen_louvain_seeds = read_seeds(settings['seed_file'], num)
    [Ci, Q, seed_count] = assign_sorted_communities_gen_louvain(A, NG, num, gamma, gen_louvain_seeds, seed_count)
    Q_real = np.mean(Q)
    print 'Q_real: ', Q_real


    avg_random_Qs_list = np.zeros(len(random_networks))

    for i in range(0,len(random_networks)):  
         Q_random, _ = calculate_modularity(random_networks[i], random_networks_NG[i], gamma, seed)
         avg_random_Qs_list[i] = Q_random
    
    avg_random_Qs = np.mean(avg_random_Qs_list)
    std_Q= np.std(avg_random_Qs_list)
    t_bounds = t.interval(0.95, len(avg_random_Qs_list) - 1) #student ts
    conf_Q = t_bounds[1]*(std_Q / np.sqrt(len(avg_random_Qs_list))) #95% confidence   
    #num_communities = max(Ci)
    num_communities = round(np.mean(Ci.max(axis=1)))

    return Q_real,avg_random_Qs,conf_Q, num_communities
