"""
Module for parsing fragment level and binned counts files.
"""

import numpy as np
from threednetmod.set_nans_qnorm import set_nans


def load_counts(countsfile, primermap, force_nan='always'):
    """
    Loads the counts values from a primer-primer pair .counts file into square,
    symmetric arrays and returns them.

    Parameters
    ----------
    countsfile : str
        String reference to location of .counts file to load counts from.
    primermap : Dict[str, List[Dict[str, Any]]]
        The keys of the outer dict are region names. The values are lists, where
        the :math:`i` th entry represents the :math:`i` th primer in that
        region. Primers are represented as dicts with the following structure::

            {
                'chrom' : str,
                'start' : int,
                'end'   : int
            }

        See ``lib5c.parsers.primers.get_primermap()``.
    force_nan : Optional[str]
        If 'always' is passed and if the primermap contains orientation
        information, impossible ligations will be always set to nan. If
        'implicit' is passed, impossible ligations will be set to nan when
        implied by the orientation information in the primermap, but not when
        the ligations are explicitly present in the countsfile. If 'never' is
        passed, orientation information will be ignored and impossible ligations
        will not be identified.

    Returns
    -------
    Dict[str, np.ndarray]
        The keys are the region names. The values are the arrays of counts
        values for that region. These arrays are square and symmetric.
    """
    # initialize counts arrays
    counts = {}
    for region in primermap.keys():
        counts[region] = np.zeros([len(primermap[region]),
                                   len(primermap[region])])

    # set nan's for 'implicit' mode
    if force_nan == 'implicit':
        set_nans(counts, primermap)

    # create reverse lookup table
    reverse_map = {primermap[region][index]['name']: (region, index)
                   for region in primermap.keys()
                   for index in range(len(primermap[region]))}

    # parse countsfile
    with open(countsfile, 'r') as handle:
        for line in handle:
            # skip comments
            if line.startswith('#'):
                continue

            # parse line information
            name1 = line.split('\t')[0].strip()
            name2 = line.split('\t')[1].strip()
            value = float(line.split('\t')[2])

            # skip unrecognized primers
            if name1 not in reverse_map or name2 not in reverse_map:
                continue

            # identify indices and regions involved in this interaction
            region1, index1 = reverse_map[name1]
            region2, index2 = reverse_map[name2]

            # skip trans interactions
            if not region1 == region2:
                continue

            # record value
            counts[region1][index1, index2] = value
            counts[region1][index2, index1] = value

    # set nan's for 'always' mode
    if force_nan == 'always':
        set_nans(counts, primermap)

    return counts
