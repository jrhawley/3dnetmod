import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

def make_is_plots(settings, df, category_names, condition_name_map, is_window_size, tags_DBR):

    out_dir = 'output/DBR/IS_plots/'
    # plot result by condition
    print 'plotting results by condition'
    sys.stdout.flush()
    
    for condition in condition_name_map.values():
        plt.clf()
        sns.set_style("white")
        sns.set_palette(sns.xkcd_palette(['navy blue', 'purple', 'sea green',
                                          'beige', 'orange', 'green',
                                          'steel grey']))
        ax=sns.pointplot(
            x='Bins from boundary', y='Insulation score',
            hue='Boundary category', data=df[df['Condition'] == condition],
            hue_order=category_names[1:-2])
        #if is_window_size in ylims:
            #plt.ylim(ylims[is_window_size])
        #plt.gcf().set_size_inches((12, 8))
        ax.grid(False)
        plt.gca().legend_.remove()
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        sns.despine()
        plt.savefig(out_dir + condition + '_' + tags_DBR + '.png', bbox_inches='tight')
            #'%s/%s_%02i.%s' % (out_dir, condition, is_window_size, '.png'),
            #bbox_inches='tight')

    # plot result by boundary category
    print 'plotting results by boundary category'
    sys.stdout.flush()
    for category_name in category_names:
        if len(df[df['Boundary category'] == category_name].index) == 0:
            continue
        plt.clf()
        sns.set_style("white")
        sns.set_palette(
            sns.xkcd_palette(['navy blue', 'orange', 'green']))
        ax=sns.pointplot(x='Bins from boundary', y='Insulation score',
                      hue='Condition',
                      data=df[df['Boundary category'] == category_name])
        ax.grid(False)
        sns.despine()
        plt.savefig(out_dir + condition + '_' + category_name.replace(' ', '_') + '_' + tags_DBR + '.png', bbox_inches='tight')
            #'%s/%s_%02i.%s'
            #% (out_dir, category_name.replace(' ', '_'), is_window_size, '.png'),
            #bbox_inches='tight')

        # plot with fixed y axis 700 to 1200 for paper
        plt.clf()
        sns.set_style("white")
        sns.set_palette(
            sns.xkcd_palette(['navy blue', 'orange', 'green']))
        ax=sns.pointplot(x='Bins from boundary', y='Insulation score',
                      hue='Condition',
                      data=df[df['Boundary category'] == category_name])
        ax.grid(False)
        plt.ylim((700,1200))
        #if is_window_size in ylims:
            #plt.ylim(ylims[is_window_size])
        #plt.gcf().set_size_inches((12, 8))
        #if category_name != 'CTCF Brd2 co-occupied':
        #    plt.gca().legend_.remove()
        #else:
        #    plt.legend(loc=3, handletextpad=0.1, fontsize=22, borderpad=0.1,
        #               borderaxespad=0.1, markerscale=1.2)
        sns.despine()
        plt.savefig(out_dir + '700_1200y_' + condition + '_' + category_name.replace(' ', '_') + '_' + tags_DBR + '.png', bbox_inches='tight')




