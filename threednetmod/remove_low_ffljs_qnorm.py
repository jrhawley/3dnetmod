import os
import sys
#import multiprocessing
import glob
import numpy as np
from threednetmod.remove_primer_primer_pairs import remove_primer_primer_pairs
from threednetmod.qnorm_counts_superdict import qnorm_counts_superdict
from threednetmod.load_primermap import load_primermap

def remove_low_ffljs_qnorm(settings):
	threshold = float(settings['qnorm_thresh'])
	primermap = load_primermap('input/' + settings['bed_file'])
	chroms = np.load('input/' + settings['sample_1'] + '.npz').keys()

	conditions = [settings['sample_1'], settings['sample_2'], settings['sample_3'], settings['sample_4']]

	file_check = 0
	for chrom in chroms:
		for condition in conditions:
			fname = 'input/%s_qnormed_%s_%s.npy' % (condition, chrom, settings['qnorm_thresh'])
			if os.path.isfile(fname):
				file_check +=1
	if file_check == len(chroms)*len(conditions): # if qnorming has been done, no need to repeat
		print 'qnorming has already been done'
		return None

	"""
	counts = {}	
	for condition in conditions:
		counts[condition]={}
		for chrom in chroms:
			print 'loading contact matrices for condition %s %s'\
				% (condition, chrom)
			sys.stdout.flush()
			data = np.load('input/' + condition + '.npz')
			counts[condition][chrom] = data[chrom]
			data.close()
	"""

	for chrom in chroms:
		counts = {}
		for condition in conditions:
			if condition not in counts:
				counts[condition] = {}
				print 'loading contact matrices for condition %s %s'\
					% (condition, chrom)
				sys.stdout.flush()
				data = np.load('input/' + condition + '.npz')
				counts[condition][chrom] = data[chrom]
				data.close()

		
		removed_counts = remove_primer_primer_pairs(counts, primermap, count_threshold = threshold, num_reps =1)
		qnormed_counts_superdict = qnorm_counts_superdict(removed_counts, primermap, tie = 'average')
		for condition in conditions:
			print 'writing qnormed counts for condition %s' % condition
			sys.stdout.flush()
			np.save('input/%s_qnormed_%s_%s.npy' % (condition, chrom, settings['qnorm_thresh']),
				qnormed_counts_superdict[condition][chrom])
			if settings['plots'] == 'True':
				print 'flattening matrices for condition %s' % condition
				sys.stdout.flush()
				data = flatten_counts_to_list(qnormed_counts_superdict[condition])
				print 'plotting distributions for condition %s' % condition
				plt.clf()
				sns.distplot(data[(data > 0) & (data < 25)], kde=False, bins=50)
				plt.savefig('output/DBR/qnorm/distributions/distribution_qnormed_%s_%s_%s.png'
					% (chrom, condition, settings['qnorm_thresh']), bbox_inches='tight')
					
					
	"""
	#counts = {condition: {chrom: counts[condition]} for condition in counts}, {chrom: primermap[chrom]},
        # remove low counts
        print 'removing low counts'
        sys.stdout.flush()
	removed_counts = remove_primer_primer_pairs(counts, primermap, count_threshold = threshold, num_reps =1)

	# quantile normalize
	print 'quantile normalizing'
	qnormed_counts_superdict = qnorm_counts_superdict(counts, primermap, tie = 'average')
	for condition in conditions:
		for chrom in qnormed_counts_superdict[condition]:
			# write counts
			print 'writing qnormed counts for condition %s' % condition
			sys.stdout.flush()
			np.save('input/%s_qnormed_%s_%s.npy' % (condition, chrom, settings['qnorm_thresh']),
				qnormed_counts_superdict[condition][chrom])
			if settings['plots'] == 'True':
				print 'flattening matrices for condition %s' % condition
				sys.stdout.flush()
				data = flatten_counts_to_list(qnormed_counts_superdict[condition])
				print 'plotting distributions for condition %s' % condition
				sys.stdout.flush()

				plt.clf()
				sns.distplot(data[(data > 0) & (data < 25)], kde=False, bins=50)
				plt.savefig('output/DBR/qnorm/distributions/distribution_qnormed_%s_%s_%s.png'
					% (chrom, condition, settings['qnorm_thresh']), bbox_inches='tight')
	"""

	return None	
