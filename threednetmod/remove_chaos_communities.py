from __future__ import division, absolute_import, print_function
from threednetmod.read_settings import read_settings
from threednetmod.create_tags import create_tags
import sys
import os
import glob
from threednetmod.load_intervals import load_intervals, load_stripped_intervals
from threednetmod.check_intersect import check_intersect
from threednetmod.get_overlap import get_overlap

def remove_chaos_communities(settings, tag_GPS, tag_HSVM):
    print('in module')
    final_communities = []
    buff = 4 # number of bins buffer for removing communities due to chaos


    samples = []
    for sample_inst in settings:
        if sample_inst[:7] == 'sample_':
            samples.append(settings[sample_inst])
    samples = sorted(samples)
    for sample in samples:
        files = glob.glob('output/HSVM/variance_thresholded_communities/merged/Merged_' + 'Communities_*' + sample  + '_' + tag_HSVM + '.txt')
        print('HSVM files:')
        print(files)
        print('looking for: ', 'output/HSVM/variance_thresholded_communities/merged/Merged_' + 'Communities_*' + sample  + '_' + tag_HSVM + '.txt')

        # chaos regions
        #output/GPS/chaos_regions/chr18.23WT2Neuronalchr18100_50_True_False_mm10_cent_tel_genedesert_False_True_0.01_chr18_1_0.0048_None_3_1.txt
        for file in files:
            final_communities = []
            print('file: ')
            print(file)
            chr = file.split('/')[-1].split('_')[2].split(sample)[0]
            community_list = load_intervals(file)
            print('chr: ', chr)
            chaos_files = glob.glob('output/GPS/chaos_regions/' + chr + '*' + sample +  tag_GPS + '.txt')
            if len(chaos_files) < 1:
                if settings['scale'] == 'genomewide':
                    pre_threshold_file = 'output/GPS/chaos_regions/' + chr + '*' + sample +  tag_GPS + '.txt'
                    pre_threshold_file = pre_threshold_file.replace('_' + 'genomewide', '_' + str(chr))
                    chaos_files = glob.glob(pre_threshold_file)
            print('looking for chaos file: ')
            print('output/GPS/chaos_regions/' + chr + '*' + sample +  tag_GPS + '.txt')
            print('chaos_files: ', chaos_files)
            chaos_regions = []
            for chaos_file in chaos_files:
                print('chaos file in for loop')
                chrom = chaos_file.split('/')[-1].split('_')[0].split(sample)[0].split('.')[0]
                if chrom == chr:
                    chaos = load_stripped_intervals(chaos_file)
                    chaos_regions += chaos
            print('chaos_regions: ', chaos_regions)
            print('community_list: ', community_list)
            for community in community_list:
                counter = 0
                for chaos_region in chaos_regions:
                    overlap = get_overlap(community,chaos_region)

                    if (abs(chaos_region['start'] - community['start']) <= buff*(int(settings['boundary_buffer']))) and (abs(chaos_region['end'] - community['end']) <= buff*(int(settings['boundary_buffer']))): # keep community if it has similar coordinates as the chaos region (it's likely an outer boundary to the chaos region)
                        continue
                    elif (community['start'] < chaos_region['start']) and (community['end'] > chaos_region['end']): # if chaos region is inside the community
                        continue
                    elif overlap >= buff*(int(settings['boundary_buffer'])): # if community overlaps with chaos region significantly, we want to remove it
                        counter +=1
                        break
                    elif (community['end'] - community['start'] < buff*(int(settings['boundary_buffer']))) and check_intersect(community,chaos_region):
                        print('lost community to final condition')
                        print("(community['end'] - community['start'] < buff*(int(settings['boundary_buffer']))) and check_intersect(community,chaos_region)")
                        print(community)
                        print(chaos_region)
                        counter +=1
                        break
                    else:
                        continue
                if counter == 0:
                    final_communities.append(community)
                    print('appended final community')
                else:
                    print('lost community to chaos')


            dir = 'output/HSVM/chaos_filtered_communities/'
            if not os.path.isdir(dir): os.makedirs(dir)
            out_file = open(dir + 'Merged_Communities_' + chr + sample + tag_HSVM + '.txt', 'w')
            for item in final_communities:
                output = item['chrom'] + '\t' + str(item['start']) + '\t' + str(item['end']) + '\t' + str(item['left_var']) + '\t' + str(item['right_var'])
                #print("chaos call = ", output)
                print(output, file=out_file)
            out_file.close()


def main():

    settings = read_settings(sys.argv[1])
    tags = create_tags(sys.argv[1])
    tags_HSVM = tags[3]
    tag_GPS = tags[1]
    remove_chaos_communities(settings, tag_GPS, tags_HSVM)


if __name__ == '__main__':
    main()
