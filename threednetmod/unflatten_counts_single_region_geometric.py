from math import sqrt
import numpy as np

def unflatten_counts_single_region(flat_regional_counts):
    """
    Turn a list of flattened counts back into a square symmetric array.

    Parameters
    ----------
    flat_regional_counts : list of floats
        A flat, nonredundant lists of counts. The ``(i*(i+1) / 2 + j)`` th element
        of this list will end up at both the ``(i, j)`` th and the ``(j, i)`` th
        element of the returned array.

    Returns
    -------
    2d numpy array
        A square, symmetric array representation of the counts. The
        ``(i*(i+1) / 2 + j)`` th element of ``flat_regional_counts`` will end up
        at both the ``(i, j)`` th and the ``(j, i)`` th element of this array.
    """
    size = int(0.5 * (-1 + sqrt(8*len(flat_regional_counts) + 1)))
    regional_counts = np.zeros([size, size])
    for i in range(size):
        for j in range(i+1):
            regional_counts[i, j] = flat_regional_counts[i * (i + 1) // 2 + j]
            regional_counts[j, i] = flat_regional_counts[i * (i + 1) // 2 + j]
    return regional_counts
