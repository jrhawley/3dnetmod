def reverse_chromosomal_primermap_by_bin_start(chromosomal_primermap):
    #print 'chromosomal_primermap: '
    #print chromosomal_primermap
    reverse_chromosomal_primermap = {chromosomal_primermap[i]['start']: dict(chromosomal_primermap[i],   **{'index': i}) for i in range(len(chromosomal_primermap))}
    print 'reverse_chromosomal_primermap: '
    print reverse_chromosomal_primermap

    return reverse_chromosomal_primermap

def get_matrix_indices_for_boundaries(boundary_positions, chromosomal_primermap):
    reversed_chromosomal_primermap = reverse_chromosomal_primermap_by_bin_start(chromosomal_primermap)
    #print '==============='
    #print 'chromosomal_primermap'
    #print chromosomal_primermap
    #print '================'
    #print 'reversed_chromosomal_primermap'
    #print reversed_chromosomal_primermap
    #print '================='
    #print 'reversed_chromosomal_primermap keys: '
    #print reversed_chromosomal_primermap.keys()
    #print typo
    print 'chromosomal_primermap[0]'
    print chromosomal_primermap[0]
    print 'boundary_positions:'
    print boundary_positions
    for pos in boundary_positions:
        print 'pos: '
        print pos
	if pos in reversed_chromosomal_primermap:
		print 'pos in reveresed chromosomal primermap'

    print 'reversed_chromosomal_primermap'
    print "[(pos, reversed_chromosomal_primermap[pos]['index']) for pos in boundary_positions if pos in reversed_chromosomal_primermap]"
    print [(pos, reversed_chromosomal_primermap[pos]['index']) for pos in boundary_positions if pos in reversed_chromosomal_primermap]
    print zip(*[(pos, reversed_chromosomal_primermap[pos]['index'])
                 for pos in boundary_positions
                 if pos in reversed_chromosomal_primermap])
    return zip(*[(pos, reversed_chromosomal_primermap[pos]['index'])
                 for pos in boundary_positions
                 if pos in reversed_chromosomal_primermap])
