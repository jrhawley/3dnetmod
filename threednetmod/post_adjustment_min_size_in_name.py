from threednetmod.read_settings import read_settings
from heatmap_removal_genomewide_mean_var import heatmap_removal
from threednetmod.load_region_start_end_coord import load_region_start_end_coord
from boundary_removal_from_bed_genomewide_min_size_in_name import boundary_removal_from_bed
import multiprocessing
import numpy as np
import sys 
import glob
import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from threednetmod.find_bad_regions_mean_var import find_bad_regions
from threednetmod.load_region_overlap_coord_handle_bad_regions import load_region_overlap_coord_handle_bad_regions


def main():

     settings = read_settings(sys.argv[1])

     bed_file = 'input/' + settings['bed_file']
     #bed_file = 'input/chr1_' + settings['sample_1'] + 'final.bed'
     region_idx, bin_size = load_region_start_end_coord(bed_file)


     total_badregions = [] #repeats allowed
     total_regions = []
     samples= []
     samples.append(settings['sample_1'])
     if 'sample_2' in settings:
          samples.append(settings['sample_2'])
     if 'sample_3' in settings:
          samples.append(settings['sample_3'])
     if 'sample_4' in settings:
          samples.append(settings['sample_4'])
     if 'sample_5' in settings:
          samples.append(settings['sample_5']) 
     if 'sample_6' in settings:
          samples.append(settings['sample_6']) 
     print 'samples: ', samples     

     # Find bad regions
     bad_region_file = 'output/bad_region_removal/' + 'bad_regions_' + settings['scale'] + '_' + settings['diagonal_density'] + '_' + settings['consecutive_diagonal_zero'] + '_' + settings['mean_var_threshold'] + '_meanvar.txt'
     if os.path.isfile(bad_region_file):
         print 'found bad region file'

         bad_regions = []
         input = open(bad_region_file, 'r')
         for line in input:
             bad_regions.append(line.strip().split())
         input.close()
         #total_flatbad = bad_regions[:]
         total_flatbad = [val for sublist in bad_regions for val in sublist]
     else:
         bad_regions = find_bad_regions(settings, samples)
         total_flatbad = bad_regions[:]

     for sample in samples:
          pre_threshold_file = 'Communities_' + settings['num_part'] + '_partitions_' + settings['pctile_threshold'] + '_' + settings['pct_value'] + '_trash_community_removal_' + '*' + str(sample)  + '_gammastep_' + settings['gamma_step']  + '_hierarchy_' + settings['hierarchies'] + settings['gamma_part'] + '_gammapart_' + settings['plateau'] + '_plateau.txt'

          dir = 'output/unique_communities/results_files/'

          if  not os.path.isdir(dir):
               print "No communities directory present"
               return 0

          existence = glob.glob(dir + pre_threshold_file)
          if len(existence) == 0:
               print "No intermediate file present. exiting"
               return 0
    
          for  existing_file in existence:
               print 'existing_file: ', existing_file

               cell_type = existing_file.split('/')[-1].split('_')[8]
               print 'cell_type: ', cell_type
               cell_type_input = cell_type.replace(sample,'') + "_" + sample

               prefinalcommunity = []

               input = open(existing_file, 'r')
               for line in input:
                    if line.startswith('#'):
                         continue
                    else:
                         #print 'line: ', line
                         region_inst = line.strip().split('\t')[0]
                         gamma_inst = line.strip().split('\t')[1]
                         hierarchy = line.strip().split('\t')[2]
                         chromosomes = line.strip().split('\t')[3]
                         start_inst = line.strip().split('\t')[4]
                         stop_inst = line.strip().split('\t')[5]
                         left_var_inst = line.strip().split('\t')[6]
                         right_var_inst = line.strip().split('\t')[7]
                         if (int(stop_inst) - int(start_inst)) // int(bin_size) > int(settings['size_threshold']):
                              if region_inst not in total_flatbad:  #remove calls from bad regions
                                   prefinalcommunity.append({'region':region_inst,'gamma':gamma_inst,'number':hierarchy,'chr':chromosomes,\
                                                  'start':start_inst,'stop':stop_inst,'left_var':left_var_inst,'right_var':right_var_inst})

                              else:
                                   print 'community in bad region!'
               
               regions = []
               community_dict = {}
               #print "length of prefinal = ",len(prefinalcommunity)
               for community in prefinalcommunity:
                    region = community['region']
                    if region not in regions:
                         regions.append(region)
                         community_dict[region] = []
                    community_dict[region].append(community)
               #print 'community_dict keys: '
               #print 'community_dict[HiCchr3019]: '
               #print community_dict['HiCchr3019']
               for keys in community_dict:
                    print keys

               print 'COMMUNITY_DICT: ', community_dict

               if len(community_dict) > 0: #do not look at empty outputs
                    variance_threshold = boundary_removal_from_bed(settings,community_dict,cell_type_input,cell_type)

     chromosomes = []
     settings_repeat = []
     condition_repeat = []
     bad_regions = []
     key_name = settings['sample_1']
     values = glob.glob('input/*' + str(key_name) + '*final.bed')
     dir = 'output/heatmaps_w_communities/'
     if not os.path.isdir(dir): os.makedirs(dir)
     acceptable = ["genomewide","chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8","chr9","chr10","chr11",\
                                   "chr12","chr13","chr14","chr15","chr16","chr17","chr18","chr19","chr20","chr21",\
                                   "chr22","chr23","chr24","chr25","chr26","chr27","chr28","chr29","chr30","chr31",\
                                   "chr32","chr33","chr34","chr35","chr36","chr37","chr38","chr39","chr40","chr41",\
                                   "chr42","chr43","chr44","chr45","chr46","chr47","chr48","chr49","chr50","chrX","chrY"]
     if settings['scale'] not in acceptable:
          print "User did not provide acceptable format for chromosome in ",acceptable
          print "Must adjust to correct format in list and/or correct input bed and counts that have format"
          return 0
     if settings['scale'] == 'genomewide':
          for lines in values:
               pre = lines.strip().split('/')
               ext = pre[1].split('_')
               chr = ext[0]  #chr in prefix separated by "_"
               if chr not in chromosomes:
                    if chr != 'allchr':
                        chromosomes.append(chr)
     else:
          print "found ", settings['scale']
          chromosomes.append(settings['scale'])
     print "chromosomes = ", chromosomes
     for i in range(0,len(chromosomes)):
          settings_repeat.append(settings)
          bad_regions.append(total_flatbad)
if __name__ == "__main__":
      main()                       
