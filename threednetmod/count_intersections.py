def check_intersect(point, feature, within=0):
    """
    Checks to see if a point intersects an feature

    Parameters
    ---------
    point: int
        The point.
    feature : Dict[str, Any]
        The feature, represented as a dict with at least the following keys::

            {
                'start': int,
                'end': int
            }

        This feature is assumed to be zero-indexed and half-open.
    within : int
        The point and the feature will be considered to intersect if they are
        within this many bp of each other.

    Returns
    -------
    bool
        True if the point intersects the feature, False otherwise.
    """
    if feature['end'] + within > point >= feature['start'] - within:
        return True
    return False

def count_intersections(query_point, feature_set, within):
    """
    Counts the number of times a query point is hit by a set of intervals.

    Parameters
    ----------
    query_point : int
        The point to count intersections for
    feature_set : List[Dict[str, Any]]
        The set of features to intersect with the query feature.
    within : int
        The point and a feature will be considered to intersect if they are
        within this many bp of each other.


    Returns
    -------
    int
        The number of intersections

    Notes
    -----
    Features are represented as dicts with the following structure::

            {
                'chrom': str
                'start': int,
                'end'  : int,
            }

    See ``lib5c.parsers.bed.load_features()``.
    """
    counter = 0
    for feature in feature_set:
        if check_intersect(query_point, feature, within=within):
            counter += 1
    return counter
