from threednetmod.rank_data import *
from threednetmod.average_rows import *
from threednetmod.sub_in_normed_val import *


def qnorm(data, tie='lowest', reference_index=None):
    """
    Quantile normalizes a data set.

    Parallelizable if ``data`` is a 2d np.ndarray; see
    ``lib5c.algorithms.qnorm.qnorm_parallel()``.

    Parameters
    ----------
    data : 2d numeric structure, or dict of 1d numeric structure
        Anything that can be cast to array. Should be passed as row-major.
        Quantile normalization will performed on the columns of ``data``.
    tie : {'lowest', 'average'}, optional
        Pass ``'lowest'`` to set all tied entries to the value of the lowest
        rank. Pass ``'average'`` to set all tied entries to the average value
        across the tied ranks.
    reference_index : int or str, optional
        If ``data`` is a row-major array, pass a column index to serve as a
        reference distribution. If ``data`` is a dict, pass a key of that dict
        that should serve as the reference distribution. Pass None to use the
        average of all distributions as the target distribution.

    Returns
    -------
    2d numpy array, or dict of 1d numpy array
        The quantile normalized data. If ``data`` was passed as a dict, then a
        dict with the same keys is returned.

    Notes
    -----
    This function is nan-safe. As long as each column of the input data
    contains the same number of nan's, nan's will only get averaged with other
    nan's, and they will get substituted back into their original positions. See
    the Examples section for an example of this.

    Examples
    --------
    >>> import numpy as np
    >>> from lib5c.algorithms.qnorm import qnorm
    >>> qnorm(np.array([[5,    4,    3],
    ...                 [2,    1,    4],
    ...                 [3,    4,    6],
    ...                 [4,    2,    8]]))
    ...
    array([[ 5.66666667,  4.66666667,  2.        ],
           [ 2.        ,  2.        ,  3.        ],
           [ 3.        ,  4.66666667,  4.66666667],
           [ 4.66666667,  3.        ,  5.66666667]])
    >>> qnorm(np.array([[     5, np.nan,      3],
    ...                 [     2,      1,      4],
    ...                 [np.nan,      4,      6],
    ...                 [     4,      2, np.nan]]))
    ...
    array([[ 5.        ,         nan,  2.        ],
           [ 2.        ,  2.        ,  3.33333333],
           [        nan,  5.        ,  5.        ],
           [ 3.33333333,  3.33333333,         nan]])
    >>> qnorm(np.array([[     5, np.nan,      3],
    ...                 [     2,      1,      4],
    ...                 [np.nan,      4,      6],
    ...                 [     4,      2, np.nan]]), reference_index=1)
    ...
    array([[  4.,  nan,   1.],
           [  1.,   1.,   2.],
           [ nan,   4.,   4.],
           [  2.,   2.,  nan]])
    >>> qnorm({'A': [5, 2, 3, 4],
    ...        'B': [4, 1, 4, 2],
    ...        'C': [3, 4, 6, 8]})
    ...
    {'A': array([ 5.66666667,  2.        ,  3.        ,  4.66666667]),
     'C': array([ 2.        ,  3.        ,  4.66666667,  5.66666667]),
     'B': array([ 4.66666667,  2.        ,  4.66666667,  3.        ])}
    >>> qnorm({'A': [5, 2, 3, 4],
    ...        'B': [4, 1, 4, 2],
    ...        'C': [3, 4, 6, 8]}, reference_index='C')
    ...
    {'A': array([ 8.,  3.,  4.,  6.]),
     'C': array([ 3.,  4.,  6.,  8.]),
     'B': array([ 6.,  3.,  6.,  4.])}
    >>> qnorm({'A': [5, 2, 3, 4],
    ...        'B': [4, 1, 4, 2],
    ...        'C': [3, 4, 6, 8]}, reference_index='C', tie='average')
    ...
    {'A': array([ 8.,  3.,  4.,  6.]),
     'C': array([ 3.,  4.,  6.,  8.]),
     'B': array([ 7.,  3.,  7.,  4.])}
    """
    # handle input types
    if type(data) == dict:
        # convert dict to array, storing replicate names for later
        replicate_order = data.keys()
        data_array = np.array([data[rep] for rep in replicate_order])
        if reference_index is not None:
            reference_index = replicate_order.index(reference_index)
    else:
        # make sure we have a numpy array otherwise
        data_array = np.array(data, dtype=float).T

    # quantile normlize.
    sorted_data, ranks, tied_rank_idx = rank_data(data_array)
    average_col = average_rows(sorted_data, reference_index=reference_index)
    normed_data = sub_in_normed_val(sorted_data, ranks, average_col,
                                     tied_rank_idx, tie)

    # handle output types
    if type(data) == dict:
        # if data was a dict then return a dict
        normed_data = np.array(normed_data, dtype=float).T
        normed_data = {replicate_order[i]: normed_data[:, i]
                       for i in range(len(replicate_order))}
    else:
        # coerce the output to a numpy array
        normed_data = np.array(normed_data, dtype=float).T

    return normed_data