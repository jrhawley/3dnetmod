import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as pyplot
import os

def make_Q_v_gamma_plots(settings, cell_type, gammas, real_Qs_dict, random_Qs_dict):
        dir = 'output/modularity/scatterplots'
        if not os.path.isdir(dir): os.makedirs(dir)
        mean_diffs_dict = {}
        normed_diffs_dict = {}
        for region in real_Qs_dict:
		# plot real and rand Qs on same plot
                if eval(settings['plots']):
                     filename = 'Q_v_gamma_' + cell_type + region  + '_step_size_gamma_' + settings['gamma_step'] +  str(eval(settings['switch'])) + '_adj_' + settings['gamma_part'] + '_gammapart.png'
                     xticks = np.arange(min(gammas[region]), max(gammas[region])+ float(settings['gamma_step']) / 2, float(settings['gamma_step'])*3)
                     real_handle, = pyplot.plot(gammas[region], real_Qs_dict[region], 'ro-', ms=6, label='Q real network')
                     rand_handle, = pyplot.plot(gammas[region], random_Qs_dict[region], 'bo-', ms=6, label='mean Q of random network')
                     pyplot.errorbar(gammas[region], random_Qs_dict[region], marker ='o')
                     pyplot.legend(handles=[real_handle,rand_handle])
                     pyplot.title('Modularity, Q')
                     pyplot.xlabel('gammas')
                     pyplot.ylabel('Q')
                     pyplot.xticks(xticks, xticks, rotation='vertical')
                     pyplot.savefig(os.path.join(dir,filename), dpi = 300, bbox_inches = 'tight')
                     pyplot.clf()
                     pyplot.close()

                filename = 'Q_v_gamma_real_minus_random' + cell_type + region  + '_step_size_gamma_' + settings['gamma_step']  + str(eval(settings['switch'])) + '_adj_' + settings['gamma_part'] + '_gammapart.png'
                mean_diffs = np.asarray(real_Qs_dict[region]) - np.asarray(random_Qs_dict[region])
                normed_diffs = mean_diffs / max(mean_diffs)

                if eval(settings['plots']):
                     xticks = xticks = np.arange(min(gammas[region]), max(gammas[region])+ float(settings['gamma_step']) / 2, float(settings['gamma_step'])*3)
                     pyplot.plot(gammas[region], normed_diffs, 'bo-', ms=6)
                     pyplot.title('Q_values real minus random normalized to max')
                     pyplot.xlabel('gammas')
                     pyplot.ylabel('Fraction of max difference')
                     pyplot.xticks(xticks, xticks, rotation='vertical')
                     pyplot.savefig(os.path.join(dir, filename), dpi = 300,  bbox_inches='tight')
                     pyplot.close()
                     pyplot.clf()

		mean_diffs_dict[region] = mean_diffs
		normed_diffs_dict[region] = normed_diffs

	return mean_diffs_dict, normed_diffs_dict


