from __future__ import division, absolute_import, print_function
import numpy as np
from threednetmod.load_primermap_qnorm import load_primermap
from threednetmod.load_counts_qnorm import load_counts
from threednetmod.remove_primer_primer_pairs_qnorm import remove_primer_primer_pairs
from threednetmod.write_counts_qnorm import write_counts

def process2(element):
    # load data
    chr = element[0]
    bed = element[1]
    counts_1 = element[2]
    sample_1 = element[3]
    counts_2 = element[4]
    sample_2 = element[5]
    counts_3 = element[6]
    sample_3 = element[7]
    counts_4 = element[8]
    sample_4 = element[9]
    tag = element[10]
    #counts_5 = element[10]
    #sample_5 = element[11]
    #counts_6 = element[12]
    #sample_6 = element[13]

    print('loading data')
    primermap = load_primermap('input/' + chr + '_' + tag + '_' + bed)
    counts_superdict = {
                sample_1 + chr + '_' + tag: load_counts('input/' + chr + '_' + tag + '_' + counts_1, primermap),
                sample_2 + chr + '_' + tag: load_counts('input/' + chr + '_' + tag + '_' + counts_2, primermap),
                sample_3 + chr + '_' + tag: load_counts('input/' + chr + '_' + tag + '_' + counts_3, primermap),
                sample_4 + chr + '_' + tag: load_counts('input/' + chr + '_' + tag + '_' + counts_4, primermap),
                #sample_5 + chr: load_counts('input/' + chr + '_' + counts_5, primermap),
                #sample_6 + chr: load_counts('input/' + chr + '_' + counts_6, primermap)
            }

    print('removing primer-primer pairs with low counts')
    trimmed_counts_superdict = remove_primer_primer_pairs(counts_superdict,primermap,
                                                          count_threshold=0.01,num_reps=1,
                                                          inplace=True)
    print('writing output')
    for rep in trimmed_counts_superdict.keys():
        write_counts(trimmed_counts_superdict[rep],'input/%s_primer_primer_pair_removed.counts' % rep,
                     primermap)

if __name__ == "__main__":
    process2()
