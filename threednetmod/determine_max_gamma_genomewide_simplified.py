from threednetmod.construct_nulls import construct_NG_null
from threednetmod.get_random_Qs import get_random_Qs
from threednetmod.real_random_Qs import real_random_Qs
from threednetmod.counts_to_array import counts_to_array
import numpy as np
import os
import glob
import sys

#def determine_max_gamma(settings,regions,counts_as_arrays,cell_type,counts,badregions,tag):
def determine_max_gamma(settings, good_regions, cell_type, tag):
    consideration_size = 30 # number of regions considered for max gamma


def determine_max_gamma(elements):
    settings = elements[0]
    regions = elements[1]
    counts_as_arrays = elements[2]
    cell_type = elements[3]
    counts = elements[4]
    badregions = elements[5]
    tag = elements[6]

    #consideration_size = (float(150) / (float(settings['overlap']) + float(settings['stride'])))*10 #number of regions based on 10 regions of 6MB 40000 bin
    consideration_size = 10

    finalized_regions = []       
    real_Qs_dict = {}
    random_Qs_dict = {}
    random_Qconfs_dict = {}
    mean_diffs_dict = {}
    gammas = {}
    random_networks_NG = {}
    max_gamma_list = []
    dist_regions_of_interest = []

    dir = 'output/GPS/gamma_dynamic_range/'
    if not os.path.isdir(dir): os.makedirs(dir)
    #file = dir + 'max_gamma_'+ '_' + settings['rate'] + '_' + cell_type + '_'+ settings['gamma_step'] + '_stride_' + settings['stride'] + '_overlap' + settings['overlap'] + '.txt'
    file = dir + 'max_gamma_*' + cell_type + '*_'+ tag

    existing_random_networks = glob.glob(file)
    print 'exisisting_random_networks: ', existing_random_networks
    if len(existing_random_networks) > 0:
        print 'already found max gamma'
        return max(np.loadtxt(existing_random_networks[0]))

    for i in range(0,len(regions)):
        if (regions[i][-3:] != '001') and (regions[i] not in badregions): #remove first region from list (shorter than rest, do not want to consider)
            finalized_regions.append(regions[i])
  
    if len(finalized_regions) > consideration_size: #if number of regions is greater than max_gamma determination
        for k in range(0,int(np.floor(consideration_size))):
            index_of_interest = int(np.floor(k * (len(finalized_regions) // consideration_size))) #spread finding max gamma equally across length of 
            dist_regions_of_interest.append(finalized_regions[index_of_interest])
    elif len(finalized_regions) == 0:
        print 'no regions to consider'
        return None
    else:
        for k in range(0,len(finalized_regions)):
            dist_regions_of_interest.append(finalized_regions[k])

    print "using regions for max gamma determination: ",dist_regions_of_interest
    if len(dist_regions_of_interest) > 0:
        for inst in dist_regions_of_interest:
            random_networks = get_random_Qs(settings, counts_as_arrays, cell_type,inst,tag) #construct random networks
            for i in range(0,len(random_networks)):
                random_networks_NG[i] = []
                random_networks_NG[i] =  construct_NG_null(random_networks[i]) #find null matrix

            A = counts_as_arrays[inst]
            np.fill_diagonal(A, 0) # zero out the diagonal of the network to match random rewiring scenario
            NG = construct_NG_null(A) # construct the Newman  Girvan null model
            real_Qs_dict[inst] = []
            random_Qs_dict[inst] = [] 
            random_Qconfs_dict[inst] = []
            mean_diffs_dict[inst] = []
            gammas[inst] = []
            gamma = 0.0
            counter = 0
            real_Q = 1
            avgdiff_change = 10
            counterlist = []
            historylist = []
            size = 10
            particular_gamma = 0
            maxcounter = 1
            max_change = -1 #neg ensures that max gets replaced by first encounter            

            while (((avgdiff_change > (float(settings['rate'])) or avgdiff_change < 0))): #cutoff at steadystate difference or random crosses paths with  real, removed requirement     to be below 0.05
                real_Q, random_Qs, conf_Q, num_communities = real_random_Qs(settings,A,NG,random_networks,random_networks_NG,gamma)
                diff = real_Q - random_Qs
                counterlist.append(counter)
                historylist.append(diff)
                history = np.abs(historylist[-1*int(size):]) #capture last 10 diffs
                if gamma >= 1.5: # assume that convergence has to occur atleat past 1.5 gamma
                    if diff < 0:
                        max_gamma_list.append(gamma) #just pick gamma as real and random cross paths after 1.5 (to stabilize must go past 1.5)
                        particular_gamma = gamma
                        break
                    maxtest = max(historylist) #retrieve largest positive (careful not to use abs)
                    if maxtest > max_change:
                        max_change = maxtest
                        for i, j in enumerate(historylist):
                            if j == maxtest:
                                maxcounter = i
                    diffwmax = max_change - historylist[-1] #diff between largest encountered and current beyond 1.5 gamma
                    adjustmentpergamma = (float(counter) - float(maxcounter))
                    if int(counter) > int(maxcounter):
                        avgdiff_change = float(diffwmax) / adjustmentpergamma
                        if avgdiff_change <= float(settings['rate']):
                            max_gamma_list.append(gamma)
                            particular_gamma = gamma
                    if np.sum(np.abs(np.diff(history))) / float((size-1)) < float(settings['rate']): # find mean of change in difference over history, converge if below 0.005 change in difference per 0.1 gamma (one less diff than size of window (size -1))
                        avgdiff_change = float(settings['rate']) / 10 #ensure convergence
                        max_gamma_list.append(gamma)
                        particular_gamma = gamma
                if real_Q < -0.1: # stop if real modularity below -0.1
                    max_gamma_list.append(gamma)
                    particular_gamma = gamma
                    break
                
              
                counter = counter + 1
                real_Qs_dict[inst].append(real_Q)
                random_Qs_dict[inst].append(random_Qs)
                random_Qconfs_dict[inst].append(conf_Q)
                gammas[inst].append(gamma)
                mean_diffs_dict[inst].append(diff)
                gamma = gamma + 0.1

            #file3 = dir + 'testing_gamma_info_' + cell_type + '_' + str(inst) + '_' + tag #Temporary debugging
            #output = open(file3, 'w') #Temp
            #header = '#Real\tRandom\tRandom_conf\tgammas\tmean_diffs'  #Temp
            #print >> output, header #Temp
            #for w in range(0,len(real_Qs_dict[inst])):   #Temp
            #    temp = '%f\t%f\t%f\t%f\t%f' %(real_Qs_dict[inst][w],random_Qs_dict[inst][w],random_Qconfs_dict[inst][w],gammas[inst][w],mean_diffs_dict[inst][w])
            #    print >> output, temp #Temp
  
            #output.close() #Temp

            #ensure that max_gamma is not empty for given region
            print "max_gamma_list ", max_gamma_list
            if len(max_gamma_list) == 0:
                if gamma > 0:
                    max_gamma_list.append(gamma - 0.1)
                else:
                    max_gamma_list.append(gamma) 
            elif particular_gamma == 0: #still overlooked
                max_gamma_list.append(gamma - 0.1)

        #use max gamma from list of regions 
        #print "max gamma = ",max(max_gamma_list)
        np.savetxt(file,max_gamma_list)    
    
        # save name of regions used to consider max gamma:
        file2 = dir + 'regions_for_determining_max_gamma_' + cell_type + '_' + tag
        output = open(file2, 'w')
        for region in dist_regions_of_interest:
             print >> output, region
        output.close()
        if len(max_gamma_list) >= 1:
    	    return max(max_gamma_list)
        else:
            return None
    else:
        return None
