from threednetmod.convert_comm_coord_to_array_chr import convert_comm_coord_to_array_chr

def convert_community_list_to_partition_list_chr(community_list, chr_map, chr):
        partitions_list = []
        for community in community_list:
                if chr == community['chr']:
                        community_array = convert_comm_coord_to_array_chr(community, chr_map)
                        partitions_list.append(community_array)
        return partitions_list
