from threednetmod.narrowpeak_line_parser import *
from threednetmod.macs_line_parser import *
from threednetmod.bed6_line_parser import *
from threednetmod.bed5_line_parser import *
from threednetmod.bedgraph_line_parser import *
from threednetmod.bed4_line_parser import *
from threednetmod.bed3_line_parser import *


def guess_field_parser(line):
    """
    One of two functions that "guesses" which BED file parser to use in order to
    load BED file features

    Parameters
    ----------
    line : str
        Represents the ith row of the BED file

    Returns
    -------
    function *_line_parser
        Appropriate parser function for extracting BED file features
    """
    parsers = [narrowpeak_line_parser, macs_line_parser, bed6_line_parser,
               bed5_line_parser, bedgraph_line_parser, bed4_line_parser,
               bed3_line_parser]
    for parser in parsers:
        try:
            parser(line)
            return parser
        except (ValueError, IndexError):
            pass
