import numpy as np


def rank_data(data):
    """
    Sorts data, finds rank, and finds all ties in each column.
    Accepted inputs are dicts and lists of lists - data should be n x m
    structure.
    Returns ranked data, list of ranks, and start and end point of all ties.

    Parameters
    ----------
    data : 2d numpy array
        The data to rank.

    Returns
    -------
    tuple of (list of 1d array, 2d array, list of list of numeric)
        The first element of the tuple is a column-sorted representation of
        ``data``. The second element of the tuple contains the column ranks of
        the entries of ``data``. The third element of the tuple contains the
        indices that separate runs of ranks that have the same value.
    """
    sorted_data = []
    tied_rank_idx = []
    ranks = np.zeros((len(data), len(data[0])))

    col_num = -1
    # sort data and find ties in each column
    for column in data:
        col_num += 1
        sort_idx = np.argsort(column)
        # get indices to revert sorted back to unsorted order
        rev_sort_idx = np.zeros(len(column), dtype='int32')
        rev_sort_idx[sort_idx] = np.arange(len(column), dtype='int32')
        sorted_col = np.asarray(column)
        sorted_col = sorted_col[sort_idx]

        sorted_data.append(list(sorted_col))

        # find ties by seeing how many consecutive sorted entries have same
        # value
        # get start site and end site, where end site is first entry that does
        # not have the same value as previous entry (or entries)
        # note: even entries with unique values are considered ties for these
        # purposes (i.e. a tie between one object)
        ind_start = 0  # start of current value
        ind_value = sorted_col[0]  # current value
        tied_rank_idx.append([])
        final_idx = len(column)-1
        for i in range(1, final_idx):
            if sorted_col[i] != ind_value:
                # assign all tied entries the same value
                ranks[col_num][ind_start:i] = ind_start
                # add start site of current tie
                tied_rank_idx[col_num].append(ind_start)
                ind_value = sorted_col[i]
                ind_start = i
        # handle final case separately
        if sorted_col[final_idx] != ind_value:
            ranks[col_num][ind_start:final_idx] = ind_start
            ranks[col_num][final_idx] = final_idx
            tied_rank_idx[col_num].append(ind_start)
            tied_rank_idx[col_num].append(final_idx)  # final value start site
            tied_rank_idx[col_num].append(final_idx+1)  # final value end site
        else:
            ranks[col_num][ind_start:final_idx+1] = ind_start
            tied_rank_idx[col_num].append(ind_start)
            tied_rank_idx[col_num].append(final_idx+1)

        # gives order of ranks for original column
        ranks[col_num] = ranks[col_num][rev_sort_idx]

    return sorted_data, ranks, tied_rank_idx
