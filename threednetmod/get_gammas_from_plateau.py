from threednetmod.get_plateaus import get_plateaus
import numpy as np

def get_gammas_from_plateau(filename, settings):
	input = open(filename, 'r')
	gammas = []
	num_comm = []
	for line in input:
		if line.startswith('#'):
			continue
		else:
			pieces = line.strip().split('\t')
			gammas.append(float(pieces[0]))
			num_comm.append(float(pieces[1]))

	plateau = get_plateaus(num_comm, gammas, int(settings['plateau']))

	final_gammas = []
	for avg_num_comm in plateau:
		if avg_num_comm > 1:
			final_gammas.append(np.mean(plateau[avg_num_comm]))



	return final_gammas

