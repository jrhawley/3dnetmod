import numpy as np

def compute_insulation_score(matrix, index, size, offset=0, data_needed=1.0):
    values = matrix[max(index - size - offset, 0):
                    max(index - offset, 0),
                    min(index + offset + 1, len(matrix)):
                    min(index + size + offset + 1, len(matrix))]
    if np.isfinite(values).sum() < data_needed*values.size:
        return np.nan
    return np.nansum(values)


def compute_insulation_score_window(matrix, index, size, window_radius,
                                    diag_offset=0, data_needed=1.0,
                                    offsets=None):
    if offsets is None:
        offsets = range(-window_radius-1, window_radius+1)
    return np.array([compute_insulation_score(matrix, index + offset, size,
                                              offset=diag_offset,
                                              data_needed=data_needed)
                     for offset in offsets])


def compute_insulation_score_even(matrix, index, size, offset=0,
                                  data_needed=1.0):
    values = matrix[max(index - size - offset, 0):
                    max(index - offset, 0),
                    min(index + offset, len(matrix)):
                    min(index + size + offset, len(matrix))]
    if np.isfinite(values).sum() < data_needed * values.size:
        return np.nan
    return np.nansum(values)


def compute_insulation_score_window_even(matrix, index, size, window_radius,
                                         diag_offset=0, data_needed=1.0,
                                         offsets=None):
    if offsets is None:
        offsets = range(-window_radius, window_radius+1)
    return np.array(
        [compute_insulation_score_even(matrix, index + offset, size,
                                       offset=diag_offset,
                                       data_needed=data_needed)
         for offset in offsets])

def compute_insulation_score_matrix(counts, boundary_indices, size,
                                    window_radius):
    return np.vstack(
        [compute_insulation_score_window(
            counts[chrom], boundary_indices[chrom][i], size, window_radius)
         for chrom in sorted(boundary_indices.keys())
         for i in range(len(boundary_indices[chrom]))])


