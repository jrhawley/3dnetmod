from __future__ import division
import pyBigWig
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pybedtools import BedTool
import os
import sys
import glob
from threednetmod.read_settings import read_settings
from threednetmod.create_tags import create_tags

def generate_axis_label(window,bins):
	#window, bins should be same as entered into get_pileup
	axis_label = range(-window, 0, int(2*window // bins)) + range(int(2*window // bins), window + int(2*window // bins), int(2*window // bins))
	return axis_label

def ctcf_piler(ctcf, boundaries, window, bins, output):
	#bins = # bins per side
	#window = window length per side
	ctcf = BedTool(ctcf)
	boundaries=pd.read_csv(boundaries, sep='\t', header=None)
	#boundaries[boundaries[2]<0.05]
	boundaries[1]=boundaries[1]-window
	boundaries[2]=boundaries[1]+2*window
	boundaries = boundaries[(boundaries >= 0).all(1)]
	#print 'boundaries: ', boundaries
	regions=BedTool.from_dataframe(boundaries)
	print 'regions: ', regions
	windows=regions.window_maker(n=2*bins, b=regions)
	#print 'windows: '
	#print windows
	windows_count=windows.intersect(ctcf, c=True).to_dataframe()
	#print 'windows_counts: '
	#print windows_count
	result=np.reshape(np.array(windows_count['name']), (2*bins, len(boundaries)), order='F').sum(axis=1) // len(boundaries)## INT-DIVISION? ##

	axis_label = generate_axis_label(window, 2*bins)
	plt.plot(axis_label, result)
	plt.savefig(output)
	plt.clf()
	plt.close()

def ctcf_piler_three(ctcf1, ctcf2, ctcf3, label1, label2, label3, boundaries, window, bins, output):
	inputf = open(boundaries, 'r')
	count = 0
	for line in inputf:
		count +=1
	print "COUNT: ", count
	inputf.close()
	if count <=5:
		return None
	ctcf1 = BedTool(ctcf1)
	ctcf2 = BedTool(ctcf2)
	ctcf3 = BedTool(ctcf3)
	print 'boundaries: '
	print boundaries
	print 'ctcf1: ', ctcf1
	print 'ctcf2: ', ctcf2
	print 'ctcf3: ', ctcf3
	boundaries = pd.read_csv(boundaries, sep='\t', header=None)
	print 'number of boundaries: ', len(boundaries)
	#boundaries = boundaries[boundaries[2]<pvalue]
	print 'loaded boundaries: ', boundaries
	print 'boundaries: ', boundaries
	#boundaries[boundaries[2]<pvalue]
        boundaries[1]=boundaries[1]-window
        boundaries[2]=boundaries[1]+2*window
	#boundaries[1]=boundaries[1]-window // 2
	#boundaries[1]=boundaries[1]+window
	print 'boundaries before greater than zero: ', boundaries
	boundaries = boundaries[(boundaries >= 0).all(1)]
	print 'boundaries after greater than zero: ', boundaries
	print 'boundaries[1]', boundaries[1]
	print 'boundaries[2]', boundaries[2]
	print 'boundaries', boundaries
        regions=BedTool.from_dataframe(boundaries)
	print 'regions: ', regions
        windows=regions.window_maker(n=2*bins, b=regions)
	#windows = regions.window_maker(n=bins, b=regions)
	print 'windows: ', windows
	windows_count1=windows.intersect(ctcf1, c=True).to_dataframe()
	windows_count2=windows.intersect(ctcf2, c=True).to_dataframe()
	windows_count3=windows.intersect(ctcf3, c=True).to_dataframe()
	result1 = np.reshape(np.array(windows_count1['name']), (2*bins, len(boundaries)), order='F').sum(axis=1) / len(boundaries) ## INT-DIVISION? ##
	result2 = np.reshape(np.array(windows_count2['name']), (2*bins, len(boundaries)), order='F').sum(axis=1) / len(boundaries) ## INT-DIVISION? ##
	result3 = np.reshape(np.array(windows_count3['name']), (2*bins, len(boundaries)), order='F').sum(axis=1) / len(boundaries) ## INT-DIVISION? ##
	#fig = plt.figure(figsize=(10, 5))
	fig = plt.figure()
	axis_label = generate_axis_label(window, 2*bins)
	#axis_label = generate_axis_label(window, bins)
	#ylim = 
	#ax1 = plt.subplot(221)
	#ax2 = plt.subplot(223)
	ax3 = plt.subplot(111)
	box = ax3.get_position()
	ax3.set_position([box.x0, box.y0, box.width*0.8, box.height])
        #ax1.plot(axis_label,result1, color='green')
        #ax2.plot(axis_label,result2, color='blue')
        #ax3.plot(axis_label,result1, color='green')
        ax3.plot(axis_label,result2, color='blue')
	#ax3.plot(axis_label, result3, color = 'red')
	ax3.legend([label1, label2, label3], loc = 'center left', bbox_to_anchor=(1.0, 0.5))
	#ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        ax3.set_ylim([0, 0.6])

	#plt.tight_layout()
        plt.savefig(output, bbox_inches = 'tight')
        plt.clf()
        plt.close()


def main():
	#settings = read_settings(sys.argv[1])
	#tags = create_tags(sys.argv[1])
	#tags_HSVM = tags[3]
	#tags_DBR = '_'.join([
	# 	tags_HSVM,
	# 	settings['qnorm_thresh'],
	# 	settings['boundary_size'],
	# 	str(int(int(settings['IS_min_dist']) // 1000)) + 'kb',
	# 	str(int(int(settings['IS_max_dist']) // 1000)) + 'kb'
	#])

	#window = sys.argv[1]
	#bin_size = sys.argv[2]
	window = 500000
	bin_size = 40000
	#window = int(window)*1000 # total plot area
	#bin_size = int(bin_size)*1000
	bins = window * 2 // bin_size
	#WT_CTCF = 'input/chip/Galaxy6-[WT_specific_CTCF].bed'
	#celltype1_CTCF = 'input/ChIP_peaks/' + settings['Boundaryclass_Celltype1']
	celltype1_CTCF = 'input/ChIP_peaks/qnormed_CTCF_1383_WT_peakcalls.bed3.bed'
	#celltype2_CTCF = 'input/ChIP_peaks/' + settings['Boundaryclass_Celltype2']
	#constitutive_CTCF = 'input/ChIP_peaks/' + settings['Boundaryclass_Constitutive']
	#KD_CTCF = 'input/chip/Galaxy5-[KO_specific_CTCF].bed'
	#constitutive_CTCF = 'input/chip/Galaxy10-[Constitutive_CTCF].bed'
	#WT_H3K27ac = 'input/chip/qnormed_H3K27ac_2058_WT_peakcalls.bed'
	#KD_H3K27ac = 'input/chip/qnormed_H3K27ac_2060_KO_peakcalls.bed'
	#WT_specific_boundaries = '6bin_SS_WT1_v_CKKO1_s_boundaries_FDR_0_05.txt'
	#KD_specific_boundaries = '6bin_SS_CKKO1_v_WT_s_boundaries_FDR_0_05.txt'


	#test_direction1 = settings['sample_3'][:-1] + '_v_' + settings['sample_1'][:-1]
	#test_direction2 = settings['sample_1'][:-1] + '_v_' + settings['sample_3'][:-1]
	#category_name = settings['sample_3'] +   '_specific_only_'

	#boundary_files = ['output/DBR/significant_boundaries_' + test_direction1 + '_' + category_name  + '_' + settings['sample_3'] + 'specific_CTCF.txt', 'output/DBR/significant_boundaries_' + test_direction2 + '_' + category_name + '_' + settings['sample_3'] + 'specific_CTCF.txt']
	#boundary_files = ['output/DBR/sig_boundaries_' + test_direction1 + '_' + category_name + '_' + settings['sample_3'] + 'spec_CTCF.txt', 'output/DBR/sig_boundaries_' + test_direction2 + '_' + category_name + '_' + settings['sample_3'] + 'spec_CTCF.txt']
	boundary_files = glob.glob('output/HSVM/sorted_by_variance/*WT1*')
	dir = 'input/'
	out_dir = 'output/DBR/pileups/'
	if not os.path.isdir(out_dir): os.makedirs(out_dir)
	#boundary_files = ['input/significant_KO_boundaries.txt']
	for boundary_f in boundary_files:
		out_dir = 'output/HSVM/pileups/'
		if not os.path.isdir(out_dir): os.makedirs(out_dir)
		#ctcf_piler_three(WT_CTCF, KD_CTCF, constitutive_CTCF, 'WT_CTCF', 'KD_CTCF', 'constitutive_CTCF', boundary_f, window, bins, out_dir + boundary_f.split('/')[-1] + '_CTCF_' + str(window // 1000) + 'kbplot_' + str(bin_size // 1000)  + '_kbbin.png')
		ctcf_piler_three(celltype1_CTCF, celltype1_CTCF, celltype1_CTCF, 'CTCF', 'CTCF', 'CTCF', boundary_f, window, bins, out_dir + boundary_f.split('/')[-1] + '_CTCF_' + str(window // 1000) + 'kbplot_' + str(bin_size // 1000)  + '_kbbin.png')


		#ctcf_piler_three(WT_H3K27ac, KD_H3K27ac, WT_H3K27ac, 'WT_H3K27ac', 'KD_H3K27ac', 'WT_H3K27ac', boundary_f, window, bins, out_dir + boundary_f.split('/')[-1] + '_H3K27ac_' + str(window // 1000) + 'kbplot_' + str(bin_size // 1000)  + '_kbbin.png')









if __name__ == '__main__':
	main()
