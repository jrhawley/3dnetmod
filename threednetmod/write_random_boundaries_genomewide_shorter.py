import numpy as np
from get_chrom_idx import get_chrom_idx
import sys
from threednetmod.check_intersect import check_intersect
from threednetmod.load_region_start_end_coord import load_region_start_end_coord
from threednetmod.read_settings import read_settings


def write_random_boundaries_no_background(all_communities_path, boundary_size, bed_path):
	#Nestin  1.50    30      chr3    87989087        88041087        0.0     0.0     1.0     0.380681818182  1.0     0.380681818182
	region_idx, bin_size  = load_region_start_end_coord(bed_path)
	input = open(all_communities_path, 'r')
	names = []
	regions = []
	boundaries_per_region = {}
	true_boundary_coords = []
	for line in input:
		if line.startswith('#'):
			continue
		else:
                        pieces = line.strip().split('\t')
			boundary1 = {}
			region = pieces[0]
			boundary1['region'] = pieces[0]
                        boundary1['chrom'] = pieces[3]
			boundary1['start'] = int(pieces[4]) - boundary_size // 2
                        boundary1['end'] = int(pieces[4]) + boundary_size // 2
			name = boundary1['chrom'] + '_' + str(boundary1['start'])
			if name not in names:
                        	true_boundary_coords.append(boundary1)
				names.append(name)

			boundary2 = {}
                        boundary2['region'] = pieces[0]
                        boundary2['chrom'] = pieces[3]
			boundary2['start'] = int(pieces[5]) - boundary_size // 2
                        boundary2['end'] = int(pieces[5]) + boundary_size // 2
			name = boundary2['chrom'] + '_' + str(boundary2['start'])

                        if region not in regions:
                                boundaries_per_region[region] = []
                                regions.append(region)

                        if name not in names:
                                true_boundary_coords.append(boundary2)
                                names.append(name)
				boundaries_per_region[region].append(boundary1)
				boundaries_per_region[region].append(boundary2)

	print 'regions: '
	print regions
	random_boundaries = []
	for region in boundaries_per_region:
		num_boundaries = len(boundaries_per_region)
		bin_options = np.arange(int(region_idx[region]['start']) // bin_size, int(region_idx[region]['end']) // bin_size + 1)

                #for i in range(num_boundaries*2): # multiply by 2 just to have extra boundaries in the list
		for i in range(100):
                        bin = np.random.permutation(bin_options)[0]
                        start_coord = bin*bin_size - boundary_size // 2
                        end_coord = bin*bin_size + boundary_size // 2
                        background = {}
			chrom = region_idx[region]['chrom']
                        background['chrom'] = chrom
                        background['start'] = start_coord
                        background['end'] = end_coord
                        random_boundaries.append(background)
		
				
	background_coords_filtered = []
        for background in random_boundaries:
                count = 0
                for true_boundary in true_boundary_coords:
                        if check_intersect(background, true_boundary):
				print 'overlaps with boundary!'
                                count +=1

                if count == 0:
                        background_coords_filtered.append(background)

	random_boundary_file = open('input/random_boundary_coords_Communities_20_partitions_1_0.80_trash_community_removal_0.96_2_bad_regions_ALL_COMMUNITIES_gammastep_0.01_hierarchy_no1_gammapart_8_plateau_thresholded_post_writing_100_90_100_100_0.txt'+ str(boundary_size) + '.txt', 'w')

        for background in background_coords_filtered:
                chrom = background['chrom']
                coord = background['start'] + boundary_size // 2
                output = chrom + "\t" + str(coord)
                print >> random_boundary_file, output
        random_boundary_file.close()


def main():

	all_communities_path, boundary_size = sys.argv[1:]
	boundary_size = int(boundary_size)
	bed_file = 'input/allchr_Cortex1final.bed'
	#write_random_boundaries(all_boundaries_path, bin_size)
	write_random_boundaries_no_background(all_communities_path, boundary_size, bed_file)


if __name__ == '__main__':
	main()
