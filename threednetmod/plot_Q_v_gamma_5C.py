import numpy as np
import matplotlib.pyplot as plt
import os



def main():
	region_1 = np.loadtxt('output/GPS/gamma_dynamic_range/Real_random_modularity_v_gamma_chr1.34_CP_HiCchr100101_100_150_True_hg19_cent_tel_True_chr1_3_True_0.85_3.5.txt')
	dir = 'output/GPS/Q_v_gamma/'
	if not os.path.isdir(dir): os.makedirs(dir)	
	plt.clf()
	plt.plot(region_1[:,0], region_1[:,1], '-o',c='blue',label= 'real')
	plt.plot(region_1[:,0], region_1[:,2], '-o',c='red',label= 'random')
	plt.xlabel('Gamma')
	plt.ylabel('Modularity')
	plt.savefig(dir + 'chr1.34_CP_HiCchr100101_100_150_True_hg19_cent_tel_True_chr1_3_True_0.85_3.5.png')
	plt.clf()
	


if __name__ == '__main__':
	main()
