import os


def save_gammas_in_range(settings, cell_type, new_gammas,tag):
	dir = 'output/GPS/gamma_dynamic_range/'
	if not os.path.isdir(dir): os.makedirs(dir)
	for region in new_gammas:
		filename = 'gamma_dynamic_range_' + cell_type + '_' + region  + '_' + tag
		final_gammas = open(dir + filename, 'w')
		print >> final_gammas, '#gamma'
		for i in range(len(new_gammas[region])):
			print >> final_gammas, new_gammas[region][i]
		final_gammas.close()
