def default_primer_name_parser(name):
    """
    The default primer name parser.

    Parameters
    ---------
    name : str
        The name of the primer found in the appropriate column of the primer
        bedfile.

    Returns
    -------
    dict
        This dict has the following structure::

            {
                'region': str,
                'number': int,
                'name': str
            }

        These fields are parsed from the primer name.

    Notes
    -----
    You can write other name parsers to accommodate different primer naming
    conventions.
    """
    pieces = name.split('_')
    region = pieces[2]
    number = int(pieces[4].split(':')[0])
    if pieces[3] == 'FOR':
        orientation = "3'"
    elif pieces[3] == 'REV':
        orientation = "5'"
    else:
        raise ValueError('default primer name scheme violation')
    corrected_name = name.split(':')[0]
    return {'region'     : region,
            'number'     : number,
            'orientation': orientation,
            'name'       : corrected_name}
