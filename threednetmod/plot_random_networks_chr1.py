# Created 081716
# Updated 011717
# Heidi Norton
import os
from threednetmod.plot_heatmap import plot_heatmap
import glob
import csv
import numpy as np
import sys
import matplotlib as mpl
from threednetmod.flatten_counts_single_region_geometric import flatten_counts_single_region
from threednetmod.get_colormap import get_colormap

def main():

	random_networks = glob.glob('output/GPS/random_networks/*chr1.*0.85*')
        dir = 'output/random_network_heatmap/'
        if not os.path.isdir(dir): os.makedirs(dir)
        #primer_file = 'input/cortexpooled30chr6.final.bed'
	#primer_file = glob.glob('input/*chr1.*CP*100_50*bed')[0]
	#primer_file = 'input/chr1.10_CP_50_25_True_Falsefinal.bed'
	primer_file = 'input/chr1.10_CP_100_150_Truefinal.bed'
	region = 'HiCchr100028'
	for file in random_networks:
		R=np.array(list(csv.reader(open(file, "rb"),delimiter=','))).astype('float')
		heatmap_filename = file.split('/')[-1] + '.png'

		colorscale=(0.0,np.percentile(flatten_counts_single_region(R, discard_nan=True),98))
		plot_heatmap(R, dir + heatmap_filename , colorscale=colorscale, show_colorscale=True, cmap=get_colormap('obs'),region = region, primer_file = primer_file, gene_track = 'input/gene_tracks/empty_track.txt')


if __name__ == '__main__':
	main()
