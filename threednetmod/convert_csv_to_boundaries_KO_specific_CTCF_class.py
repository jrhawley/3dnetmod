from threednetmod.read_settings import read_settings
from threednetmod.create_tags import create_tags

def result_csv_to_txt(in_file, out_file, q_value_thresh):
	input = open(in_file, 'r')
	output = open(out_file, 'w')

	for line in input:
		pieces = line.strip().split(',')
		if pieces[0].startswith('chr'):
			chr = pieces[0]
			coord = pieces[1]
			qvalue = float(pieces[-2])
			if qvalue< q_value_thresh:
				temp = chr + '\t' + coord
				print >> output, temp

	output.close()
	input.close()
	

def main():
	q_value = 0.05
	settings = read_settings(sys.argv[1])
	tags = create_tags(sys.argv[1])
	tags_HSVM = tags[3]
	tags_DBR = '_'.join([
		tags_HSVM,
		settings['qnorm_thresh'],
		settings['boundary_size'],
		str(int(settings['IS_min_dist']) // 1000) + 'kb',
		str(int(settings['IS_max_dist']) // 1000) + 'kb'
	])


	category_name = settings['sample_3'] +   ' specific only '
	outdir = 'output/DBR/'
	test_direction = settings['sample_3'][:-1] + '_v_' + settings['sample_1'][:-1]
	outdir + test_direction + '_' + category_name + '_' + tags_DBR + '.csv'
	WT_file = 

	WT_file = 'input/UPDATE_FINAL_output_option2A_WT_update_peaksDI100_40kb_440kb_within40_need0.75/boundaries_by_l2fc_is_q-value_KO specific only.csv'
	KO_file = 'input/UPDATE_FINAL_output_option2A_KO_update_peaksDI100_40kb_440kb_within40_need0.75/boundaries_by_l2fc_is_q-value_KO specific only.csv'
	result_csv_to_txt(WT_file, 'input/significant_WT_boundaries_KO_specific_CTCF.txt', q_value)
	result_csv_to_txt(KO_file, 'input/significant_KO_boundaries_KO_specific_CTCF.txt', q_value)


if __name__ == '__main__':

	main()
