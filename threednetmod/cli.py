'''
cli
==========

Command line interface for 3dnetmod main command
'''

from __future__ import division, absolute_import, print_function
import argparse
import os.path as path
import threednetmod.preprocess as pre
import threednetmod.gps_mmcp as gps_mmcp
import threednetmod.hsvm as hsvm

def main():
    '''
    Main
    '''
    # acceptable chromosomes
    CHRS = ['chr' + str(i) for i in range(1, 51) + ['X', 'Y']]

    PARSER = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    # parser for subcommand choice
    SUBPARSER = PARSER.add_subparsers(
        description='Calculation step to perform',
        dest='command'
    )
    # parser for preprocessing
    PARSER_PRE = SUBPARSER.add_parser('preprocess', help='Preprocess data')
    PARSER_PRE.add_argument(
        'cfg',
        type=str,
        help='Configuration file.'
    )
    PARSER_PRE.add_argument(
        'bed',
        type=str,
        help='Name of bedfile in input directory.'
    )
    PARSER_PRE.add_argument(
        'samples',
        type=str,
        help='Comma-separated list of sample names.'
    )
    PARSER_PRE.add_argument(
        'counts',
        type=str,
        help='Comma-separated list of files for HiC counts. Must be in same order as `samples`.'
    )
    PARSER_PRE.add_argument(
        'res',
        type=int,
        help='Bin size (bp) of counts and BED file.'
    )
    PARSER_PRE.add_argument(
        'region_size',
        type=int,
        help='Size of regions into which the genome should be sliced (units of bins).',
        default=300
    )
    PARSER_PRE.add_argument(
        'overlap',
        type=int,
        help='Number of overlapping bins between adjacent regions.',
        default=200
    )
    PARSER_PRE.add_argument(
        'scale',
        type=str,
        help='Scale on which to process files.',
        default='genomewide',
        choices=['genomewide'] + CHRS
    )
    PARSER_PRE.add_argument(
        '-l', '--do-log',
        action='store_true',
        help='Input counts are integers, and they should be log-transformed.',
        default=False
    )
    PARSER_PRE.add_argument(
        '-p', '--processors',
        type=int,
        help='Number of threads for processing.',
        default=4
    )

    # parser for GPS-MMCP step
    PARSER_GPS_MMCP = SUBPARSER.add_parser('gps-mmcp', help='Perform GPS and MMCP steps')
    PARSER_GPS_MMCP.add_argument(
        'cfg',
        type=str,
        help='Configuration file'
    )

    # parser for HSVM step
    PARSER_HSVM = SUBPARSER.add_parser('hsvm', help='Perform HSVM step')
    PARSER_HSVM.add_argument(
        'cfg',
        type=str,
        help='Configuration file'
    )
    ARGS = PARSER.parse_args()
    
    if ARGS.command == 'preprocess':
        pre.main(ARGS.cfg)
    elif ARGS.command == 'gps-mmcp':
        gps_mmcp.main(ARGS.cfg)
    elif ARGS.command == 'hsvm':
        raise NotImplementedError("To be done soon, still testing")
        # hsvm.main(ARGS.cfg)

if __name__ == '__main__':
    main()
