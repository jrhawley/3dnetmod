
import numpy as np

"""
input: 
	community : dict where keys are 'region', 'start', 'end'
	bin_location : nested dictionary of bin_locations:
		bin_location[region][bin_index] = [bin_name, chr, start, end]
"""

def convert_comm_coord_to_array(community, bin_location):
        region = community['region']
        community_coordinates = (int(community['start']), int(community['end']))
        num_nodes = len(bin_location[region])
        comm_array = np.zeros((1,num_nodes)) # create an array of zeros of the size of the network 


        for index_bin in bin_location[region]:
                if bin_location[region][index_bin][2]>=community_coordinates[0] and bin_location[region][index_bin][3]<=community_coordinates[1]: # if bin start coordinate is greater than or equal to the community start coordinate AND bin end coordinate is less than or equal to the community end coordinate, the bin belongs to the community 
                        comm_array[0,index_bin] = 1

        return comm_array
