import numpy as np


def set_nans(counts, primermap):
    """
    Sets nan's in counts dict for ligations considered impossible according to a
    primermap with orientation information.

    Parameters
    ----------
    counts : Dict[str, np.ndarray]
        The keys are the region names. The values are the arrays of counts
        values for that region. These arrays are square and symmetric.
    primermap : Dict[str, List[Dict[str, Any]]]
        The keys of the outer dict are region names. The values are lists, where
        the :math:`i` th entry represents the :math:`i` th primer in that
        region. Primers are represented as dicts with the following structure::

            {
                'chrom'       : str,
                'start'       : int,
                'end'         : int,
                'orientation' : "3'" or "5'"
            }

        See ``lib5c.parsers.primers.get_primermap()``.

    Notes
    -----
    If the primermap passed has no orientation information, this function will
    do nothing.

    This function operates in-place.
    """
    for region in counts.keys():
        for i in range(len(primermap[region])):
            for j in range(i+1):
                if ('orientation' in primermap[region][i].keys() and
                            'orientation' in primermap[region][j].keys()):
                    if primermap[region][i]['orientation'] == \
                            primermap[region][j]['orientation']:
                        counts[region][i, j] = np.nan
                        counts[region][j, i] = np.nan
