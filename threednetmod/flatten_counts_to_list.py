def flatten_counts_to_list(counts, chrom_order=None, discard_nan=True,
                           num_diags_kept=None):
    if chrom_order is None:
        chrom_order = counts.keys()
    if num_diags_kept is None:
        flattened_matrices = {
            chrom: counts[chrom][
                np.tril(np.ones_like(counts[chrom], dtype=bool))]
            for chrom in chrom_order}
    else:
        flattened_matrices = {
            chrom: counts[chrom][
                np.logical_xor(np.tril(np.ones_like(counts[chrom], dtype=bool)),
                               np.tril(np.ones_like(counts[chrom], dtype=bool),
                                       k=-num_diags_kept))]
            for chrom in chrom_order}
    if discard_nan:
        return np.concatenate(
            [flattened_matrices[chrom][np.isfinite(flattened_matrices[chrom])]
             for chrom in chrom_order])
    else:
        return np.concatenate([flattened_matrices[chrom]
                               for chrom in chrom_order])
