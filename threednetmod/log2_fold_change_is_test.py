import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from statutils.qvalue import qvalue


def log2_fold_change_is_test(settings, df, conditions, samples, category_names, tags_DBR, test_direction):

	outdir = 'output/DBR/'
	sys.stdout.flush()
	for category_name in category_names:	
		grouped_df = df[(np.abs(df['Bins from boundary']) == 2) & 
				(df['Boundary category'] == category_name)].\
				groupby(['Boundary chromosome', 'Boundary coordinate',
					'Boundary category', 'Condition']).\
				agg({'Insulation score': np.nanmean}).unstack().reset_index().\
				dropna()
		if len(grouped_df.index) == 0:
			continue
		grouped_df['lambda_' + conditions[0]] = (grouped_df['Insulation score'][samples[0]] +
							grouped_df['Insulation score'][samples[1]]) / 2
		grouped_df['lambda_' + conditions[1]] = (grouped_df['Insulation score'][samples[2]] +
							 grouped_df['Insulation score'][samples[3]]) / 2


		print 'samples: ', samples
		grouped_df['Log 2 Fold-Change in IS (' + conditions[1] + '/' + conditions[0] + ')'] = np.log2(
			grouped_df['lambda_' + conditions[1]] / grouped_df['lambda_' + conditions[0]])

		grouped_df['Log 2 Fold-Change in IS (' + samples[2] + '/' + samples[3] + ')'] = np.log2(
			grouped_df['Insulation score'][samples[2]] /
			grouped_df['Insulation score'][samples[3]])


		print 'conditions: ', conditions
		grouped_df['right-tail p-value'] = None
		print 'Log 2 Fold-Change in IS (' + conditions[1] + '/' + conditions[0] + ')'
		print 'Log 2 Fold-Change in IS (' + samples[2] + '/' + samples[3] + ')'
		grouped_df.loc[np.isfinite(grouped_df['Log 2 Fold-Change in IS (' + conditions[1] + '/' + conditions[0] + ')']), 'right-tail p-value'] = 1 - (np.sum(grouped_df.loc[np.isfinite(grouped_df['Log 2 Fold-Change in IS (' + conditions[1] + '/' + conditions[0] + ')']), 'Log 2 Fold-Change in IS (' + conditions[1] + '/' + conditions[0] + ')'][:, np.newaxis] > grouped_df.loc[np.isfinite(grouped_df['Log 2 Fold-Change in IS (' + conditions[1] + '/' + conditions[0] + ')']), 'Log 2 Fold-Change in IS (' + samples[2] + '/' + samples[3] + ')'][:, np.newaxis].T, axis=1)) / float(len(grouped_df.loc[np.isfinite(grouped_df['Log 2 Fold-Change in IS (' + conditions[1] + '/' + conditions[0] + ')']), 'Log 2 Fold-Change in IS (' + samples[2] + '/' + samples[3] + ')']) + 1)

		grouped_df.dropna(inplace=True)
		if grouped_df.empty:
			print 'category %s has zero boundaries with complete IS information' % category_name
			sys.stdout.flush()
			continue
		 # append this category's boundaries to boundary_categories.csv...
		if category_name not in ['All', 'Random']:
			with open(outdir + 'boundary_categories_' + test_direction + '_' + tags_DBR + '.csv', 'a') as handle3:
			#with open('%s/boundary_categories.csv' % outdir, 'a') \
				#as handle3:

				grouped_df[['Boundary chromosome',
					'Boundary coordinate',
					'Boundary category']]\
					.to_csv(handle3, header=False, index=False)
		try:
			 # convert p-values to q-values via directFDR
			grouped_df['right-tail q-value'] = qvalue(
				 np.array(grouped_df['right-tail p-value'], dtype=float))[0]
				
		except Exception:
			print 'failed to estimate pi_0^hat in category %s information'\
				% category_name
			grouped_df['right-tail q-value'] = np.nan

		grouped_df.to_csv(outdir + test_direction + '_' + category_name + '_' + tags_DBR + '.csv', index=False)
			 #'%s/boundaries_by_l2fc_is_q-value_%s.csv' %
			#(outdir, category_name), index=False)
		
		if len(grouped_df.index) > 1:
			l2fc_is_dist_bins = np.linspace(-1.0, 1.0, 33)
			# plot histograms of l2fc
			plt.clf()
			sns.set_style("white")
			ax=sns.distplot(grouped_df['Log 2 Fold-Change in IS (' + conditions[1] + '/' + conditions[0] + ')'], kde=False, bins=l2fc_is_dist_bins)
			ax.grid(False)
			plt.xlim([-1.0, 1.0])
			sns.despine()
			plt.savefig('output/DBR/l2fc_is_dis_' + conditions[1] + '_' + conditions[0] + '_' + category_name.replace(' ', '_') + '_' + tags_DBR +  '.png')
							
			plt.clf()
			sns.set_style("white")
			ax=sns.distplot(grouped_df['Log 2 Fold-Change in IS (' + samples[2] + '/' + samples[3] + ')'], kde=False, bins=l2fc_is_dist_bins)
			ax.grid(False)
			plt.xlim([-1.0, 1.0])
			sns.despine()
			plt.savefig('output/DBR/l2fc_isdist_' + samples[2] + '_' + samples[3] + '_' + category_name.replace(' ', '_')  + '_' + tags_DBR +  '.png')
