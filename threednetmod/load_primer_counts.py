import numpy as np


def load_primer_counts(countsfile, primermap):
    """
    Loads the counts values from a primer-primer pair .counts file into square,
    symmetric arrays and returns them.
    Returns
    -------
    Dict[str, np.ndarray]
        The keys are the region names. The values are the arrays of counts
        values for that region. These arrays are square and symmetric.
    """
    # initialize counts arrays
    counts = {}
    for region in primermap.keys():
        counts[region] = np.zeros([len(primermap[region]),
                                   len(primermap[region])])

    # create reverse lookup table
    reverse_map = {primermap[region][index]['name']: (region, index)
                   for region in primermap.keys()
                   for index in range(len(primermap[region]))}

    # parse countsfile
    with open(countsfile, 'r') as handle:
        for line in handle:
            # skip comments
            if line.startswith('#'):
                continue

            # parse line information
            name1 = line.strip().split('\t')[0]
            name2 = line.strip().split('\t')[1]
            value = float(line.strip().split('\t')[2])

            # skip unrecognized primers
            if name1 not in reverse_map or name2 not in reverse_map:
                continue

            # identify indices within region
            region1, index1 = reverse_map[name1]
            region2, index2 = reverse_map[name2]

            # skip trans interactions
            if not region1 == region2:
                continue

            # record value
            counts[region1][index1, index2] = value
            counts[region1][index2, index1] = value


    return counts
