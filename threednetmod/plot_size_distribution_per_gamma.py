import sys
import os
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as pyplot
import glob

# Note: this is only meant to handle a results file with a single gamam

def main():

	gamma  = sys.argv[1]
	results_files = glob.glob('output/MMCP/communities_pre_filters/Unthresholded_communities_chr7.*CP_50_75_True_hg19_cent_tel_True_chr7_3_True_0.85_' + str(gamma) + '_20_0_0.txt')


	print 'number of files: ', len(results_files)
	color = '0.85'
	community_sizes = []
	count = 0
	for file in results_files:
		count +=1
		input = open(file, 'r')
		for line in input:
			if line.startswith('#'):
				continue
			else:
				gamma = float(line.strip().split('\t')[1])
				start = int(line.strip().split('\t')[4])
				stop = int(line.strip().split('\t')[5])
				community_size = (stop - start) // 1000
				community_sizes.append(community_size)

		input.close() 
	print '---------'
	print 'GAMMA: ', gamma
	print 'number of results files: ', count
	print 'Community sizes', community_sizes
	print 'Median community size'
	print np.median(community_sizes)
	print 'number of domains: '
	print len(community_sizes)
	print '-------------'
	#Make a histogram of community sizes
	filename = 'Community_size_distribution_Density_histogram_' + str(gamma) +  '.png'
	dir = 'output/community_size_histograms'
	if not os.path.isdir(dir): os.makedirs(dir)
	#pyplot.hist(community_sizes)
	#pyplot.hist(community_sizes, bins = 20, color = color, range = (0,7000), normed = True, linewidth = 3.0)
	#pyplot.savefig(os.path.join(dir, filename), dpi = 300, bbox_inches = 'tight')
        results, edges = np.histogram(community_sizes, bins = 20, range = (0,4000), density=True)
	#results, edges = np.histogram(community_sizes, bins = 20, density =True)
        binWidth = edges[1] - edges[0]
        #adjusted_edges = edges - binWidth // 2
        pyplot.bar(edges[:-1], results*binWidth, binWidth, color = color)
	pyplot.xlim([0,4000])
	pyplot.savefig(os.path.join(dir,filename), dpi = 300, bbox_inches = 'tight')
        pyplot.clf()
        pyplot.close()


if __name__ == '__main__':
	main()
