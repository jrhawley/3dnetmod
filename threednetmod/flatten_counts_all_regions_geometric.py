from threednetmod.flatten_counts_single_region_geometric import *

def flatten_counts_all_regions(counts, region_order=None, discard_nan=False):
    """
    Flattens counts for all regions into a single, flat, nonredudant list.

    Parameters
    ----------
    counts : dict of 2d numpy arrays
        The keys are the region names. The values are the arrays of counts
        values for that region. These arrays are square and symmetric.
    region_order : list of str
        List of string reference to region names in the order the regions should
        be concatenated when constructing the flat list. If None, this will be
        set to ``counts.keys()``.
    discard_nan : bool
        If True, nan's will not be present in the returned list.

    Returns
    -------
    (list of str, list of float) tuple
        The list of strings is the order of the regions used to concatenate the
        flattened regional counts. The list of floats contains the concatenated
        flattened regional counts. For information on the order in which
        flattened regional counts are created in, see
        ``lib5c.util.counts.flatten_regional_counts()``. If discard_nan is True,
        then the nan elements will be missing and this specific indexing will
        not be preserved.
    """
    if not region_order:
        region_order = counts.keys()
    counts_list = [x for region in region_order
                   for x in flatten_counts_single_region(counts[region],
                                                    discard_nan=discard_nan)]
    return region_order, counts_list


