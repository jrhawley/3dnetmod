import numpy as np
from threednetmod.qnorm import qnorm
from threednetmod.counts_superdict_to_matrix import counts_superdict_to_matrix
from threednetmod.unflatten_counts_all_regions_geometric import unflatten_counts_all_regions
from threednetmod.flatten_counts_single_region_geometric import flatten_counts_single_region
from threednetmod.unflatten_counts_single_region_geometric import unflatten_counts_single_region
from threednetmod.function_util import parallelize_regions


# decorates qnorm function by parallelizing
qnorm_parallel = parallelize_regions(qnorm)


def qnorm_counts_superdict(counts_superdict, primermap, tie='lowest',
                           regional=False, condition_on=None, reference=None):
    """
    Convenience function for quantile normalizing a counts superdict data
    structure.

    Parameters
    ----------
    counts_superdict : Dict[Dict[np.ndarray]]
        The keys of the outer dict are replicate names, the keys of the inner
        dict are region names, the values are square symmetric arrays of counts
        for the specified replicate and region.
    primermap : Dict[str, List[Dict[str, Any]]]
        The primermap describing the loci whose interaction counts are described
        in the ``counts_superdict``.
    tie : {'lowest', 'average'}
        Pass ``'lowest'`` to set all tied entries to the value of the lowest
        rank. Pass ``'average'`` to set all tied entries to the average value
        across the tied ranks.
    regional : bool
        Pass True to quantile normalize regions separately. Pass False to
        quantile normalize all regions together.
    condition_on : Optional[str]
        Pass a string key into the inner dicts of ``primermap`` to condition on
        that quantity. Current limitations: only works with ``regional=True``
        and can only condition with exact equality (does not support
        conditioning on strata of a quantity). Pass None to not do conditional
        quantile normalization.
    reference : Optional[str]
        Pass a string key into the ``counts_superdict`` to indicate a replicate
        that should be used as a reference distribution to quantile normalize
        to.

    Returns
    -------
    Dict[Dict[np.ndarray]]
        The keys of the outer dict are replicate names, the keys of the inner
        dict are region names, the values are square symmetric arrays of the
        quantile normalized counts for the specified replicate and region.
    """
    # determine rep order and region order
    print 'in qnorm_counts_superdict'

    rep_order = counts_superdict.keys()
    print 'rep_order: ', rep_order
    region_order = counts_superdict[rep_order[0]].keys()
    print 'region_order: ', region_order
    ref_index = rep_order.index(reference) if reference is not None else None

    if regional:
        if condition_on is None:
            # construct matrices for each region
            matrices = {
                region: np.array(
                    [flatten_counts_single_region(counts_superdict[rep][region])
                     for rep in rep_order]).T
                for region in region_order}

            # qnorm matrices
            qnormed_matrices = qnorm_parallel(matrices, tie=tie,
                                              reference_index=ref_index)

            # propagate nan's
            for region in region_order:
                qnormed_matrices[region][
                    (~np.isfinite(qnormed_matrices[region]))
                    .any(axis=1)] = np.nan

            # unpack qnormed data
            qnormed_counts_superdict = {
                rep_order[i]: {
                    region: unflatten_counts_single_region(
                        qnormed_matrices[region][:, i])
                    for region in region_order}
                for i in range(len(rep_order))}
        else:
            # collect locus property information
            properties = {region: np.array(
                [primermap[region][i][condition_on]
                 for i in range(len(primermap[region]))])
                          for region in primermap}
            unique_values = np.unique(np.concatenate(
                [properties[region] for region in region_order]))

            # enforce non-inplace operation
            qnormed_counts_superdict = {
                rep: {region: np.copy(counts_superdict[rep][region])
                      for region in region_order}
                for rep in rep_order}

            # overwrite with conditionally qnormed values
            for i in range(len(unique_values)):
                for j in range(i + 1):
                    for region in region_order:
                        sel = np.outer(
                            properties[region] == unique_values[i],
                            properties[region] == unique_values[j]
                        )
                        sel = sel | sel.T
                        if np.sum(sel):
                            matrix = np.array(
                                [qnormed_counts_superdict[rep][region][sel]
                                 for rep in rep_order]).T
                            qnormed_matrix = qnorm(matrix, tie=tie,
                                                   reference_index=ref_index)
                            for k in range(len(rep_order)):
                                qnormed_counts_superdict[rep_order[k]][region][
                                    sel] = qnormed_matrix[:, k]
    else:
        # construct matrix
        matrix = counts_superdict_to_matrix(
            counts_superdict, rep_order=rep_order, region_order=region_order).T

        # qnorm matrix
        qnormed_matrix = qnorm(matrix, tie=tie, reference_index=ref_index)

        # propagate nan's
        qnormed_matrix[(~np.isfinite(qnormed_matrix)).any(axis=1)] = np.nan

        # unpack qnormed data
        qnormed_counts_superdict = {
            rep_order[i]: unflatten_counts_all_regions(qnormed_matrix[:, i],
                                                       region_order, primermap)
            for i in range(len(rep_order))}

    return qnormed_counts_superdict
