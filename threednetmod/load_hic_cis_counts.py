#from threednetmod.get_primermap import get_primermap
from threednetmod.load_primermap import load_primermap
from threednetmod.load_primer_counts import load_primer_counts

def load_hic_cis_counts(matrix_file, bin_file):
	primermap = load_primermap(bin_file)
	counts = load_primer_counts(matrix_file, primermap)
	return counts, primermap
