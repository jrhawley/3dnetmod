from math import sqrt

import numpy as np

from threednetmod.default_bin_name_parser import default_bin_name_parser


def load_binned_counts(countsfile, name_parser=default_bin_name_parser, pixelmap=None):
    """
    Loads the counts values from a binned .counts file into square, symmetric
    arrays and returns them.

    Parameters
    ----------
    countsfile : str
        String reference to location of .counts file to load counts from.
    name_parser : Optional[Callable[[str], Dict[str, Any]]]
        Function that takes in the bin name column of the countsfile and returns
        a dict containing key-value pairs containing information required to
        identify the bin. At a minimum, this dict must have the following
        structure::

            {
                'region': str,
                'index': int
            }

        This information is necessary to deduce what region a given bin in the
        countsfile belongs to. The index key is optional, but recommended. If
        present, its value should be the zero-based index of the bin within the
        region. If not present, the pixelmap will be searched to identify the
        bin index.
    pixelmap : Optional[Dict[str, List[Dict[str, Any]]]]
        The keys of the outer dict are region names. The values are lists, where
        the :math:`i` th entry represents the :math:`i` th bin in that region.
        Bins are represented as dicts with the following structure::

            {
                'chrom': str,
                'start': int,
                'end'  : int,
                'name' : str
            }

        See ``lib5c.parsers.get_pixelmap()``. The pixelmap is used to identify
        the index of a bin within a region. If ``name_parser`` returns an index
        key, you can pass ``None`` here since the index will be determined from
        the bin name.

    Returns
    -------
    Dict[str, np.ndarray]
        The keys are the region names. The values are the arrays of counts
        values for that region. These arrays are square and symmetric.

    Notes
    -----
    This function casts the counts values in the countsfile to floats, so it
    will work even if the countsfile actually contains pseudocounts or other
    non-integer values.
    """

    # dict to store the unshaped count information
    unshaped_counts = {}

    # parse countsfile
    with open(countsfile, 'r') as handle:
        for line in handle:
            # split line on tab
            pieces = line.strip().split('\t')

            # parse bin names
            name1_fields = name_parser(pieces[0])
            name2_fields = name_parser(pieces[1])

            # deduce region
            region = name1_fields['region']

            # deduce bin1 index
            bin1 = None
            # easy way: use index returned by name_parser
            if 'index' in name1_fields.keys():
                bin1 = name1_fields['index']
            # hard way: search pixelmap for bin with this name
            else:
                for i in range(len(pixelmap[region])):
                    if pixelmap[region][i]['name'] == pieces[0]:
                        bin1 = i

            # deduce bin2 index
            bin2 = None
            # easy way: use index returned by name_parser
            if 'index' in name2_fields.keys():
                bin2 = name2_fields['index']
            # hard way: search pixelmap for bin with this name
            else:
                for i in range(len(pixelmap[region])):
                    if pixelmap[region][i]['name'] == pieces[1]:
                        bin2 = i

            # parse value
            value = float(pieces[2])

            # add region to dict if this is a new one
            if region not in unshaped_counts:
                unshaped_counts[region] = []

            # add this line to the list
            unshaped_counts[region].append({'bin1' : bin1,
                                            'bin2' : bin2,
                                            'value': value})

    # dict to store 2D arrays
    counts = {}

    for region in unshaped_counts:
        # determine the size of the 2D array we should create for this region
        # this should be the inverse function of (n**2 - n) / 2 + n
        size = 0.5 * (-1 + sqrt(8*len(unshaped_counts[region]) + 1))

        # initialize this new 2D array
        counts[region] = np.zeros([int(size), int(size)])

        # fill in this array
        for unshaped_count in unshaped_counts[region]:
            counts[region][unshaped_count['bin1'], unshaped_count['bin2']] = \
                unshaped_count['value']
            counts[region][unshaped_count['bin2'], unshaped_count['bin1']] = \
                unshaped_count['value']

    return counts
