from setuptools import setup, find_packages
from os import path

this_dir = path.abspath(path.dirname(__file__))
with open(path.join(this_dir, 'README.rst')) as f:
    long_description = f.read()

setup(
    name='3DNetMod',
    version='0.3',
    description='Sensitively detect nested, hierarchical domains in Hi-C data.',
    url='http://bibucket.org/jrhawley/3dnetmod',
    authors=['Heidi Norton', 'Daniel Emerson'],
    classifiers=[
        "Programming Language :: Python :: 2",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: MacOS",
        "Operating System :: Unix",
        "Intended Audience :: Science/Research",
        "Natural Language :: English",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ],
    packages=find_packages(),
    install_requires=[
        'numpy>=1.10.1',
        'bctpy>=0.5.0',
        'multiprocess>=0.70.4',
        'pandas>=0.15.2',
        'scikit-learn>=0.20.3',
        'patsy>=0.4.1',
        'pyparsing>=2.1.10',
        'pysam>=0.8.4',
        'python-dateutil>=2.6.0',
        'pytz>=2016.10',
        'dill>=0.2.5',
        'pyBigWig>=0.3.3',
        'seaborn>=0.7.1',
        'scipy>=0.15.1',
        'interlap>=0.2.2',
        'pybedtools>=0.7.10'
    ],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            '3dnetmod=threednetmod.cli:main'
        ]
    },
    scripts=['bin/3dnetmod'],
    zip_safe=True,
    long_description=long_description,
    long_description_content_type='text/x-rst'
)
